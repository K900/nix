let
  keys = import ../keys.nix;
in
with keys;
{
  "dns.age".publicKeys = [
    index
    k900
    konata
    morgana
    yui
    qb
  ];

  "chinatsu.age".publicKeys = [
    k900
    konata
  ];
  "shadowsocks.age".publicKeys = [
    k900
    konata
  ];
  "vaultwarden.age".publicKeys = [
    k900
    konata
  ];
  "wireguard.age".publicKeys = [
    k900
    konata
  ];
  "wireguard-psk.age".publicKeys = [
    k900
    konata
  ];

  "galene-token-key.age".publicKeys = [
    k900
    konata
  ];

  "akkoma/secret_key_base.age".publicKeys = [
    k900
    konata
  ];
  "akkoma/signing_salt.age".publicKeys = [
    k900
    konata
  ];
  "akkoma/live_view_signing_salt.age".publicKeys = [
    k900
    konata
  ];
  "akkoma/vapid_private_key.age".publicKeys = [
    k900
    konata
  ];
  "akkoma/vapid_public_key.age".publicKeys = [
    k900
    konata
  ];
  "akkoma/default_signer.age".publicKeys = [
    k900
    konata
  ];

  "grafana_client_secret.age".publicKeys = [
    k900
    qb
  ];
  "grafana_contact_points.age".publicKeys = [
    k900
    qb
  ];

  "headscale_client_secret.age".publicKeys = [
    k900
    konata
  ];

  "morgana_oauth2_proxy_secrets.age".publicKeys = [
    k900
    morgana
  ];

  "moonraker.age".publicKeys = [
    k900
    morgana
  ];

  "paperless_auth_config.age".publicKeys = [
    index
    k900
  ];

  "index_oauth2_proxy_secrets.age".publicKeys = [
    index
    k900
  ];

  "qb_oauth2_proxy_secrets.age".publicKeys = [
    k900
    qb
  ];

  "shadowsocks-client.age".publicKeys = [
    index
    k900
  ];

  "urlwatch.age".publicKeys = [
    k900
    qb
  ];

  "transmission.age".publicKeys = [
    index
    k900
  ];

  "bananya-wireless-password.age".publicKeys = [
    bananya
    k900
  ];
  "wg-bananya.age".publicKeys = [
    bananya
    k900
  ];

  "miniflux_client_secret.age".publicKeys = [
    k900
    konata
  ];
}
