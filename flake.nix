{
  description = "A mess of NixOS configs";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/staging-next";
    systems.url = "github:nix-systems/default";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };

    flake-compat = {
      url = "github:nix-community/flake-compat";
      flake = false;
    };

    # FIXME: hack to pin firefox->lib-aggregate->flake-utils
    lib-aggregate = {
      url = "github:nix-community/lib-aggregate";
      inputs.flake-utils.follows = "flake-utils";
    };

    # FIXME: hack to pin grapevine->attic->{crane,nix-github-actions}
    attic = {
      url = "github:zhaofengli/attic";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
        crane.follows = "crane";
        flake-compat.follows = "flake-compat";
        nix-github-actions.follows = "nix-github-actions";
      };
    };

    nix-github-actions = {
      url = "github:nix-community/nix-github-actions";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane.url = "github:ipetkov/crane";

    # FIXME: unpin
    firefox = {
      url = "github:K900/flake-firefox-nightly";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        lib-aggregate.follows = "lib-aggregate";
      };
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-wsl = {
      url = "github:nix-community/NixOS-WSL";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
      };
    };

    jovian = {
      url = "github:Jovian-Experiments/Jovian-NixOS";
      flake = false;
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        home-manager.follows = "home-manager";
        systems.follows = "systems";
      };
    };

    spicetify-nix = {
      url = "github:Gerg-L/spicetify-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        systems.follows = "systems";
      };
    };

    rust-cross-windows = {
      url = "gitlab:K900/rust-cross-windows";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    winhibit = {
      url = "gitlab:K900/winhibit";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pre-commit-hooks.follows = "pre-commit-hooks";
        cross.follows = "rust-cross-windows";
      };
    };

    chinatsu = {
      url = "gitlab:0upti.me/chinatsu";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    zero-uptime-api = {
      url = "gitlab:0upti.me/api";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    zero-uptime-ui = {
      url = "gitlab:0upti.me/ui";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    nix-index-database = {
      url = "github:Mic92/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    bootis = {
      url = "gitlab:K900/bootis";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    scd4x-exporter = {
      url = "gitlab:K900/scd4x-exporter";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
      };
    };

    grapevine = {
      type = "gitlab";
      host = "gitlab.computer.surgery";
      owner = "matrix";
      repo = "grapevine-fork";
      inputs = {
        attic.follows = "attic";
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        flake-compat.follows = "flake-compat";
        crane.follows = "crane";
      };
    };

    lix = {
      url = "git+https://gerrit.lix.systems/lix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pre-commit-hooks.follows = "pre-commit-hooks";
        flake-compat.follows = "flake-compat";
      };
    };

    trackerlist = {
      url = "github:ngosang/trackerslist";
      flake = false;
    };

    aagl = {
      url = "github:ezKEa/aagl-gtk-on-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
      };
    };

    plasma-manager = {
      url = "github:nix-community/plasma-manager";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        home-manager.follows = "home-manager";
      };
    };

    uboot-bpi-r4 = {
      url = "github:K900/u-boot/bpi-r4";
      flake = false;
    };

    linux-bpi-r4 = {
      url = "github:K900/linux/bpi-r4";
      flake = false;
    };

    splitbrain = {
      url = "gitlab:K900/splitbrain";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    ecuador = {
      url = "gitlab:K900/ecuador";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };

    colmena = {
      url = "github:zhaofengli/colmena";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        stable.follows = "nixpkgs";
        nix-github-actions.follows = "nix-github-actions";
        flake-compat.follows = "flake-compat";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  nixConfig = {
    extra-substituters = [
      "https://0uptime.cachix.org"
    ];
    extra-trusted-public-keys = [
      "0uptime.cachix.org-1:ctw8yknBLg9cZBdqss+5krAem0sHYdISkw/IFdRbYdE="
    ];
  };

  outputs =
    inputs:
    with import ./utils/mk-config.nix {
      hostSystem = "x86_64-linux";
      inherit inputs;

      patches =
        f: with f; {
          nixpkgs = [
            # xdg-desktop-portal update
            (npr 387894 "sha256-fJCSlT09THGgkDpt9Faw8AT3NV0M/a8kU4waeP9bPPE=")
            # mesa refactor
            (npr 387292 "sha256-evx+Oa3ZiwcmGVkHzbx/lS0Ljm9OuKeb/UpNDz+oP6U=")
          ];
          jovian = [
            # mesa refactor fixes
            (jpr 491 "sha256-iQ2AD6sBq0KRivm4DUUEcP05QqVSG9AyfRQOZSKgVzs=")
          ];
        };
    }; {
      legacyPackages.x86_64-linux = pkgs;

      devShells.x86_64-linux.default = pkgs.mkShellNoCC {
        packages = [
          inputs.agenix.packages.x86_64-linux.agenix
          inputs.colmena.packages.x86_64-linux.colmena
          pkgs.cachix
          pkgs.just
        ];

        inherit (inputs.self.checks.x86_64-linux.pre-commit-check) shellHook;
      };

      checks.x86_64-linux = {
        pre-commit-check = inputs.pre-commit-hooks.lib.x86_64-linux.run {
          src = ./.;
          hooks = {
            nixfmt-rfc-style.enable = true;
            deadnix.enable = true;
            statix.enable = true;
          };
        };
      };

      homeConfigurations = {
        # weird steam deck setup
        k900 = mkHome {
          username = "k900";
          homeDirectory = "/home/deck";
        };
      };

      nixosConfigurations = inputs.self.colmenaHive.nodes;

      allMachines =
        let
          toLink = name: value: {
            inherit name;
            path = value.config.system.build.toplevel;
          };
          links = pkgs.lib.mapAttrsToList toLink inputs.self.nixosConfigurations;
        in
        pkgs.linkFarm "all-machines" links;

      colmenaHive = inputs.colmena.lib.makeHive {
        meta = hiveMeta;

        # Home desktop
        sayaka = mkSystem {
          hostname = "sayaka";
          allowLocalDeployment = true;
        };
        sayaka-wsl = mkWslSystem {
          hostname = "sayaka";
        };

        # Personal laptop (new)
        akane = mkSystem {
          hostname = "akane";
          allowLocalDeployment = true;
        };
        akane-wsl = mkWslSystem {
          hostname = "akane";
        };

        # Personal laptop (old)
        # yuki = mkSystem { hostname = "yuki"; };
        # yuki-wsl = mkWslSystem { hostname = "yuki"; };

        # Steam Deck
        gura = mkSystem { hostname = "gura"; };

        # 0upti.me server
        konata = mkSystem { hostname = "konata"; };

        # Raspberry Pi 4 SBC
        # yui = mkSystem {
        #   hostname = "yui";
        #   system = "aarch64-linux";
        # };

        # Orange Pi 5 SBC
        qb = mkSystem {
          hostname = "qb";
          system = "aarch64-linux";
        };

        # CM3588 NAS Kit SBC
        index = mkSystem {
          hostname = "index";
          system = "aarch64-linux";
        };

        # Orange Pi 3B SBC
        morgana = mkSystem {
          hostname = "morgana";
          system = "aarch64-linux";
        };

        # Banana Pi R4 router
        bananya = mkSystem {
          hostname = "bananya";
          system = "aarch64-linux";
        };

        # family computer
        breadloaf = mkSystem { hostname = "breadloaf"; };
      };
    };
}
