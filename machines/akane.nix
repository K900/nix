{ pkgs, ... }:
let
  mapNtfs = import ../utils/map-ntfs.nix pkgs;
in
{
  imports = [ ../shared/desktop.nix ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "kvm-amd" ];

    kernelParams = [
      # Tested safe
      "pcie_aspm.policy=powersupersave"
      # FIXME: try something for https://gitlab.freedesktop.org/drm/amd/-/issues/3647
      "amdgpu.dcdebugmask=0x10"
    ];

    initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "usbhid"
      "usb_storage"
      "sd_mod"
      "sdhci_pci"
    ];

    loader.bootis = {
      extraEntries."Windows 11".loader = "/EFI/Microsoft/Boot/bootmgfw.efi";

      refind.theme.regular = {
        size = "m";
        font.size = 28;
      };
    };
  };

  hardware = {
    cpu.amd.updateMicrocode = true;
    enableRedistributableFirmware = true;
  };

  services = {
    # WMI controls
    asusd.enable = true;
    # don't probe hwmon on dGPU as that causes it to wake up
    grafana-agent.settings.integrations.node_exporter.disable_collectors = [ "hwmon" ];
    # enable Thunderbolt/USB4
    hardware.bolt.enable = true;

    # xruns with USB-C KVM otherwise
    pipewire.extraConfig = {
      pipewire."99-quantum"."context.properties"."default.clock.min-quantum" = 1024;
      pipewire-pulse."99-quantum"."pulse.properties"."pulse.min.quantum" = "1024/48000";
    };
  };

  fileSystems =
    {
      "/" = {
        device = "/dev/disk/by-label/nixos_root";
        fsType = "bcachefs";
        options = [ "compression=zstd,version_upgrade=compatible,discard" ];
      };

      "/boot" = {
        device = "/dev/disk/by-label/boot";
        fsType = "vfat";
      };
    }
    // mapNtfs {
      "c" = "windows_root";
      "d" = "data";
    };

  powerManagement.cpuFreqGovernor = "powersave";

  home-manager.users.k900.programs.plasma.input.touchpads = [
    {
      vendorId = "04f3";
      productId = "319b";
      name = "ASUE120A:00 04F3:319B Touchpad";
      naturalScroll = true;
    }
  ];
}
