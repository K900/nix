{ inputs, ... }:
{
  imports = [
    inputs.splitbrain.nixosModules.splitbrain
  ];

  services.splitbrain = {
    enable = true;
    settings = {
      bootstrap_dns = "10.11.0.1:53";

      domains = [
        { name = "bt4gprx.com"; }
        { name = "*.googlevideo.com"; }
        {
          name = "*.gamepass.com";
          action = "clear";
        }
        {
          name = "*.xboxlive.com";
          action = "clear";
        }
        {
          name = "*.xboxservices.com";
          action = "clear";
        }
        {
          name = "warframe.com";
          action = "clear";
        }
        {
          name = "cache.nixos.org";
          action = "clear";
        }
      ];

      address_lists = [
        { url = "https://antifilter.network/download/ip.lst"; }
        { url = "https://community.antifilter.download/list/community.lst"; }
      ];

      domain_lists = [
        {
          url = "https://antifilter.download/list/domains.lst";
        }
        {
          url = "https://raw.githubusercontent.com/1andrevich/Re-filter-lists/refs/heads/main/community.lst";
          preload = true;
        }
      ];
    };
  };

  systemd.services = {
    "kresd@" = {
      after = [ "splitbrain.service" ];
      overrideStrategy = "asDropin";
    };

    splitbrain = {
      after = [ "amneziawg.service" ];
      wants = [ "amneziawg.service" ];
    };
  };
}
