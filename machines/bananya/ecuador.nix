{ inputs, ... }:
{
  imports = [
    inputs.ecuador.nixosModules.default
  ];

  services = {
    ecuador.enable = true;

    grafana-agent.settings.metrics.configs = [
      {
        name = "ecuador";
        scrape_configs = [
          {
            job_name = "ecuador";
            static_configs = [ { targets = [ "localhost:9009" ]; } ];
          }
        ];
      }
    ];
  };
}
