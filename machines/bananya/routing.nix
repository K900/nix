{ pkgs, utils, ... }:
{
  networking = {
    nat = {
      enable = true;
      internalIPs = [ "192.168.0.0/16" ];
    };

    nftables = {
      enable = true;
      checkRuleset = false; # doesn't support flowtables

      tables.forward-offload = {
        name = "forward-offload";
        family = "ip"; # v4
        content = ''
          flowtable f {
            hook ingress priority 0;
            devices = { "wan", "wan-sfp" };
            counter;
            # FIXME: doesn't work yet
            # flags offload;
          }

          chain forward {
            type filter hook forward priority 0; policy accept;
            meta l4proto { tcp, udp } flow add @f;
          }
        '';
      };
    };
  };

  systemd.services.offload-bridge = rec {
    after = [
      "nftables.service"
      (utils.escapeSystemdPath "/sys/devices/virtual/net/br-lan.device")
    ];
    wants = after;

    wantedBy = [ "network.target" ];

    serviceConfig.Type = "oneshot";

    path = [ pkgs.nftables ];

    script = ''
      nft add flowtable ip forward-offload f { devices = {"br-lan"} \; }
    '';
  };
}
