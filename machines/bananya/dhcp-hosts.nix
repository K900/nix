[
  # x.0.0/24: dynamic

  # x.1.0/24: workstations
  {
    hw-address = "D8:43:AE:5D:47:C1";
    hostname = "sayaka";
    ip-address = "192.168.1.1";
  }

  {
    hw-address = "B4:8C:9D:01:F4:07";
    hostname = "akane";
    ip-address = "192.168.1.2";
  }

  {
    hw-address = "2C:3B:70:AA:14:71";
    hostname = "gura";
    ip-address = "192.168.1.3";
  }

  # x.2.0/24: servers
  {
    hw-address = "C0:74:2B:FD:CD:C4";
    hostname = "qb";
    ip-address = "192.168.2.1";
  }

  {
    hw-address = "DA:42:BF:0F:9D:1C";
    hostname = "morgana";
    ip-address = "192.168.2.2";
  }

  {
    hw-address = "A6:B4:66:5E:1D:2D";
    hostname = "index";
    ip-address = "192.168.2.3";
  }

  # x.3.0/24: media devices
  {
    hw-address = "F8:B9:5A:2A:42:5C";
    hostname = "tv";
    ip-address = "192.168.3.1";
  }

  {
    hw-address = "10:2B:41:0A:30:90";
    hostname = "soundbar";
    ip-address = "192.168.3.2";
  }

  {
    hw-address = "D4:CF:F9:C7:2E:02";
    hostname = "yyj";
    ip-address = "192.168.3.3";
  }

  {
    hw-address = "14:85:54:EE:22:78";
    hostname = "yyc";
    ip-address = "192.168.3.4";
  }

  {
    hw-address = "B8:7B:D4:96:25:15";
    hostname = "kirkwood";
    ip-address = "192.168.3.6";
  }

  # x.4.0/24: appliances
  {
    hw-address = "B8:8C:29:60:24:0A";
    hostname = "ac-livingroom";
    ip-address = "192.168.4.1";
  }

  {
    hw-address = "B8:8C:29:59:BC:34";
    hostname = "ac-bedroom";
    ip-address = "192.168.4.2";
  }

  {
    hw-address = "B8:4D:43:99:10:D9";
    hostname = "succ";
    ip-address = "192.168.4.3";
  }

  {
    hw-address = "4C:EB:D6:CB:D5:4C";
    hostname = "printer-plug";
    ip-address = "192.168.4.4";
  }

  {
    hw-address = "AE:0B:FB:EC:08:6F";
    hostname = "kettle";
    ip-address = "192.168.4.5";
  }

  {
    hw-address = "4C:BC:E9:89:15:76";
    hostname = "washer";
    ip-address = "192.168.4.6";
  }

  {
    hw-address = "E0:BB:9E:7C:C9:1B";
    hostname = "l3256";
    ip-address = "192.168.4.7";
  }

  {
    hw-address = "50:8B:B9:56:07:CD";
    hostname = "cat-fountain";
    ip-address = "192.168.4.8";
  }

  {
    hw-address = "CC:8C:BF:2D:23:F2";
    hostname = "cat-feeder";
    ip-address = "192.168.4.9";
  }

  # x.254.0/24: management ports
  {
    hw-address = "8C:A6:82:70:27:C0";
    hostname = "media-switch";
    ip-address = "192.168.254.1";
  }
]
