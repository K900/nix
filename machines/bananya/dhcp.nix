{
  services = {
    kea = {
      dhcp4 = {
        enable = true;
        settings = {
          valid-lifetime = 3600;
          renew-timer = 900;
          rebind-timer = 1800;

          lease-database = {
            type = "memfile";
            persist = true;
            name = "/var/lib/kea/dhcp4.leases";
          };

          control-socket = {
            socket-type = "unix";
            socket-name = "/run/kea/dhcp4.sock";
          };

          interfaces-config = {
            dhcp-socket-type = "raw";
            interfaces = [ "br-lan" ];

            # stupid hack to make Kea wait ~forever (really one day) for the bridge to be up
            service-sockets-max-retries = 86400000;
            service-sockets-retry-wait-time = 1000;
          };

          option-data = [
            {
              name = "routers";
              data = "192.168.0.1";
            }
            {
              name = "domain-name-servers";
              data = "192.168.0.1";
            }
            {
              name = "domain-name";
              data = "home";
            }
            {
              name = "domain-search";
              data = "home";
            }
          ];

          subnet4 = [
            {
              id = 1;
              subnet = "192.168.0.0/16";
              pools = [ { pool = "192.168.0.2 - 192.168.0.254"; } ];
              reservations = import ./dhcp-hosts.nix;
            }
          ];
        };
      };

      ctrl-agent = {
        enable = true;
        settings = {
          http-host = "127.0.0.1";
          http-port = 4000;
          control-sockets.dhcp4 = {
            socket-type = "unix";
            socket-name = "/run/kea/dhcp4.sock";
          };
        };
      };
    };

    prometheus.exporters.kea = {
      enable = true;
      controlSocketPaths = [
        "http://127.0.0.1:4000"
      ];
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "kea";
        scrape_configs = [
          {
            job_name = "kea";
            static_configs = [ { targets = [ "localhost:9547" ]; } ];
          }
        ];
      }
    ];
  };

  networking.firewall.interfaces.br-lan.allowedUDPPorts = [ 67 ];
}
