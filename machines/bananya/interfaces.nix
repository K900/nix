{ lib, ... }:
{
  systemd.network.links =
    let
      remap = iface: mac: {
        name = "50-${iface}";
        value = {
          matchConfig.MACAddress = mac;
          linkConfig.Name = iface;
        };
      };
      remapAll = lib.attrsets.mapAttrs' remap;
    in
    remapAll {
      wan-sfp = "02:57:ad:50:06:8b";
      lan-sfp = "02:57:ad:50:06:8a";
    };
}
