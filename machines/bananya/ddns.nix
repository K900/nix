{ lib, pkgs, ... }:
{
  services = {
    knot = {
      enable = true;
      settings = {
        server.listen = "127.0.0.1@5353";

        acl.dhcp_ddns = {
          address = "127.0.0.1";
          action = "update";
        };

        zone.home = {
          file =
            let
              toRecord = { hostname, ip-address, ... }: "${hostname} A ${ip-address}";
              records = map toRecord (import ./dhcp-hosts.nix);
            in
            pkgs.writeText "home.zone" ''
              @ SOA bananya me.0upti.me 0 86400 7200 4000000 11200
              @ NS bananya.home
              bananya A 192.168.0.1
              ${lib.concatStringsSep "\n" records}
            '';
          acl = [ "dhcp_ddns" ];
          zonefile-sync = "-1"; # don't write the zonefile to disk
          zonefile-load = "difference-no-serial"; # but do load from disk
          journal-content = "all"; # and write changes to journal
        };
      };
    };

    kea = {
      dhcp4.settings = {
        dhcp-ddns.enable-updates = true;
        ddns-send-updates = true;
        ddns-update-on-renew = true;
        ddns-qualifying-suffix = "home.";
      };

      dhcp-ddns = {
        enable = true;
        settings.forward-ddns.ddns-domains = [
          {
            name = "home.";
            dns-servers = [
              {
                ip-address = "127.0.0.1";
                port = 5353;
              }
            ];
          }
        ];
      };
    };
  };
}
