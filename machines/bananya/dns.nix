{ pkgs, ... }:
{
  services = {
    resolved.enable = false;

    kresd = {
      enable = true;
      package = pkgs.knot-resolver.override { extraFeatures = true; };

      listenPlain = [ "53" ];

      extraConfig = builtins.readFile ./kresd.lua;
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "kresd";
        scrape_configs = [
          {
            job_name = "kresd";
            static_configs = [ { targets = [ "localhost:8453" ]; } ];
          }
        ];
      }
    ];
  };

  networking.firewall.interfaces.br-lan.allowedUDPPorts = [ 53 ];
}
