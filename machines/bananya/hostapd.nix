{
  lib,
  pkgs,
  utils,
  ...
}:
let
  authSettings = {
    enableRecommendedPairwiseCiphers = true;
    wpaPasswordFile = "/run/agenix/wireless-password";
    saePasswordsFile = "/run/agenix/wireless-password";
  };

  bssSettings = {
    # enable 4addr mode and join the bridge
    wds_sta = 1;
    bridge = "br-lan";

    # Indoor environment only
    country3 = "0x49";

    # Stationary AP config indicates that the AP doesn't move hence location data
    # can be considered as always up to date. If configured, LCI data will be sent
    # as a radio measurement even if the request doesn't contain a max age element
    # that allows sending of such data. Default: 0.
    stationary_ap = 1;

    # okc: Opportunistic Key Caching (aka Proactive Key Caching)
    # Allow PMK cache to be shared opportunistically among configured interfaces
    # and BSSes (i.e., all configurations within a single hostapd process).
    okc = 1;
  };

  countryCode = "RU";
in
{
  age.secrets.wireless-password.file = ../../secrets/bananya-wireless-password.age;

  hardware.wirelessRegulatoryDatabase = true;

  environment.systemPackages = [ pkgs.iw ];

  systemd = {
    services.hostapd = {
      # don't take down the network
      reloadIfChanged = true;

      # This makes hostapd_cli work, because it creates sockets in /tmp
      serviceConfig.PrivateTmp = lib.mkForce false;

      unitConfig = rec {
        # needs to be here to remove the dependencies on individual interfaces,
        # because those are now vifs
        After = lib.mkForce [ (utils.escapeSystemdPath "/sys/devices/virtual/net/br-lan.device") ];
        Wants = After;
        BindsTo = lib.mkForce [ ];
      };
    };

    network.netdevs =
      let
        mkNetdev = name: {
          netdevConfig = {
            Name = name;
            Kind = "wlan";
          };
          wlanConfig = {
            PhysicalDevice = 0;
            Type = "ap";
          };
        };
      in
      {
        wl24g = mkNetdev "wl24g";
        wl5g = mkNetdev "wl5g";
        wl6g = mkNetdev "wl6g";
      };
  };

  services.hostapd = {
    enable = true;

    radios = {
      wl24g = {
        band = "2g";
        channel = 0;
        inherit countryCode;

        wifi4 = {
          enable = true;
          capabilities = [
            "LDPC"
            "HT40+"
            "GF"
            "SHORT-GI-20"
            "SHORT-GI-40"
            "TX-STBC"
            "RX-STBC1"
            "MAX-AMSDU-7935"
          ];
        };

        wifi6 = {
          enable = true;
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        wifi7 = {
          enable = true;
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        networks.wl24g = {
          ssid = "Asami";
          bssid = "00:0c:43:26:60:10";
          authentication = authSettings // {
            mode = "wpa2-sha1"; # old devices don't like WPA3 (soundbar, vacuum, plug)
            enableRecommendedPairwiseCiphers = false; # printer plug does not like any ciphers but CCMP being _even present_
          };

          settings = bssSettings // {
            # disable 802.11w for old devices (soundbar, plug?)
            ieee80211w = 0;
          };
        };
      };

      wl5g = {
        band = "5g";
        channel = 0;
        inherit countryCode;

        wifi4 = {
          enable = true;
          capabilities = [
            "LDPC"
            "HT40+"
            "GF"
            "SHORT-GI-20"
            "SHORT-GI-40"
            "TX-STBC"
            "RX-STBC1"
            "MAX-AMSDU-7935"
          ];
        };

        wifi5 = {
          enable = true;
          operatingChannelWidth = "160";
          capabilities = [
            "MAX-MPDU-11454"
            "MAX-A-MPDU-LEN-EXP7"
            "VHT160"
            "RXLDPC"
            "SHORT-GI-80"
            "SHORT-GI-160"
            "TX-STBC-2BY1"
            "RX-STBC-1"
            "SU-BEAMFORMER"
            "SU-BEAMFORMEE"
            "MU-BEAMFORMER"
            "MU-BEAMFORMEE"
            "RX-ANTENNA-PATTERN"
            "TX-ANTENNA-PATTERN"
            "SOUDING-DIMENSION-2"
            "BF-ANTENNA-3"
          ];
        };

        wifi6 = {
          enable = true;
          operatingChannelWidth = "160";
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        wifi7 = {
          enable = true;
          operatingChannelWidth = "160";
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        networks.wl5g = {
          ssid = "Korra";
          bssid = "00:0c:43:26:60:b8";
          authentication = authSettings // {
            mode = "wpa3-sae-transition";
          };
          settings = bssSettings // {
            # enable RNR to advertise our 6GHz BSS on the 5GHz band
            rnr = 1;
          };
        };
      };

      wl6g = {
        # FIXME: isn't detected, investigate
        band = "5g";
        channel = 0;
        inherit countryCode;

        wifi4.enable = false;
        wifi5.enable = false;

        wifi6 = {
          enable = true;
          operatingChannelWidth = "160";
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        wifi7 = {
          enable = true;
          operatingChannelWidth = "160";
          singleUserBeamformee = true;
          singleUserBeamformer = true;
          multiUserBeamformer = true;
        };

        settings = {
          # 6 GHz Access Point type
          # This config is to set the 6 GHz Access Point type. Possible options are:
          # 0 = Indoor AP
          # 1 = Standard power AP
          # 2 = Very low power AP (default)
          # 3 = Indoor enabled AP
          # 4 = Indoor standard power AP
          # This has no impact for operation on other bands.
          he_6ghz_reg_pwr_type = 0;

          # Global operating class (IEEE 802.11, Annex E, Table E-4)
          # This option allows hostapd to specify the operating class of the channel
          # configured with the channel parameter. channel and op_class together can
          # uniquely identify channels across different bands, including the 6 GHz band.
          #
          # [This is, for some reason, required, even though hostapd has all the information
          # to figure it out. The failure mode is it looks like it works, but nothing happens.
          # 134 means 6GHz, 160MHz channel width.]
          op_class = 134;
        };

        networks.wl6g = {
          ssid = "Test 6G";
          bssid = "00:0c:43:26:60:12";
          authentication = authSettings // {
            mode = "wpa3-sae"; # hard required on 6GHz
          };
          settings = bssSettings // {
            # ieee80211w: Whether management frame protection (MFP) is enabled
            # 0 = disabled (default)
            # 1 = optional
            # 2 = required
            # [This is required by the spec for 6GHz]
            ieee80211w = 2;

            # Beacon Protection (management frame protection for Beacon frames)
            # This depends on management frame protection being enabled (ieee80211w != 0)
            # and beacon protection support indication from the driver.
            # 0 = disabled (default)
            # 1 = enabled
            # [This is required by the spec for 6GHz]
            beacon_prot = 1;
          };
        };
      };
    };
  };
}
