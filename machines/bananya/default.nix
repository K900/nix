{ pkgs, ... }:
{
  imports = [
    ../../shared/platform/bpi-r4.nix
    ../../shared/server.nix

    ../../shared/applications/monitoring

    ../../shared/applications/tailscale.nix

    ./ddns.nix
    ./dhcp.nix
    ./dns.nix
    ./downlink.nix
    ./ecuador.nix
    ./hostapd.nix
    ./interfaces.nix
    ./routing.nix
    ./splitbrain.nix
    ./uplink.nix
  ];

  networking.useDHCP = false;
  systemd.network.enable = true;

  powerManagement.cpuFreqGovernor = "ondemand";

  environment.systemPackages = [ pkgs.wol ];
}
