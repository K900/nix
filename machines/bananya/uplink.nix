{ config, pkgs, ... }:
{
  boot = {
    extraModulePackages = [ config.boot.kernelPackages.amneziawg ];
    kernelModules = [ "amneziawg" ];
  };

  age.secrets.wg-private.file = ../../secrets/wg-bananya.age;

  systemd = {
    services = {
      amneziawg = {
        after = [
          "nftables.service"
          "systemd-networkd-wait-online@wan.service"
        ];
        wantedBy = [ "network.target" ];
        wants = [ "systemd-networkd-wait-online@wan.service" ];

        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
        };

        path = [
          pkgs.amneziawg-tools
          pkgs.iproute2
          pkgs.nftables
        ];

        script = ''
          ip link add awg type amneziawg

          awg set awg \
            private-key /run/agenix/wg-private \
            jc 5 \
            jmin 40 \
            jmax 70 \
            s1 10 \
            s2 5 \
            h1 1548060210 \
            h2 1423311709 \
            h3 1803398043 \
            h4 1351438416

          awg set awg \
            peer "4qLu8VJ2Lq/4mTSx9zY8IQwAs02dTvAB0zdySf2mimo=" \
            preshared-key /run/agenix/wg-private \
            endpoint 95.216.204.176:51822 \
            persistent-keepalive 30 \
            allowed-ips 0.0.0.0/0,::/0

          nft add flowtable ip forward-offload f { devices = {"awg"} \; }
        '';

        preStop = ''
          ip link del awg
        '';
      };
    };

    network = {
      networks = {
        upstream = {
          name = "wan";
          DHCP = "ipv4";

          linkConfig.RequiredForOnline = false;

          cakeConfig = {
            Bandwidth = "1G";
            NAT = true;
            PriorityQueueingPreset = "diffserv4";
            OverheadBytes = 24;
            FlowIsolationMode = "triple";
          };
        };

        amnezia = {
          name = "awg";
          address = [
            "10.11.0.78/32"
            "fdb0:6e09:474c::4e/128"
            "fdb0:6e09:474c:4e::/64"
          ];
          routes = [
            {
              Destination = "10.11.0.0/16";
              Scope = "link";
            }
            {
              Destination = "fdb0:6e09:474c::1/48";
            }
            {
              Destination = "0.0.0.0/0";
              Table = "vpn";
              Scope = "link";
            }
            {
              Destination = "::/0";
              Table = "vpn";
            }
          ];
          routingPolicyRules = [
            {
              Priority = 1;
              FirewallMark = "0x0100/0xff00";
              Table = "vpn";
            }
          ];

          linkConfig.RequiredForOnline = false;
        };
      };

      wait-online.ignoredInterfaces = [ "awg" ];

      config.networkConfig.RouteTable = "vpn:100";
    };
  };
}
