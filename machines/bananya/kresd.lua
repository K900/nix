cache.size = 10 * MB

modules = {"policy", "serve_stale < cache", "stats", "predict", "http", "dnstap"}

http.config({
    tls = false
})
http.prometheus.namespace = 'kresd_'

net.listen('127.0.0.1', 8453, {
    kind = 'webmgmt'
})

function splitoff(name, server)
    dname = kres.str2dname(name)
    regex = string.format('^.+%s$', dname)
    policy.add(policy.pattern(policy.FLAGS({'NO_CACHE'}), regex))
    policy.add(policy.pattern(policy.STUB(server), regex))
end

splitoff('home', '127.0.0.1@5353')
splitoff('ts.0upti.me', '100.100.100.100')

policy.add(policy.all(policy.STUB('10.11.0.1')))

dnstap.config({
    socket_path = "/run/splitbrain/dnstap",
    client = {
        log_responses = true
    }
})
