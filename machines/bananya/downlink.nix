{
  systemd.network = {
    netdevs.br-lan.netdevConfig = {
      Name = "br-lan";
      Kind = "bridge";
    };

    networks = {
      lan-ports = {
        matchConfig.Name = "lan*";
        networkConfig.Bridge = "br-lan";
      };

      downstream = {
        name = "br-lan";

        linkConfig.RequiredForOnline = false;

        networkConfig = {
          Address = "192.168.0.1/16";
          ConfigureWithoutCarrier = true;
        };
      };
    };
  };
}
