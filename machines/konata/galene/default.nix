{ lib, pkgs, ... }:
let
  defaultGroupConfig = {
    public = true;
    codecs = [
      "vp8"
      "vp9"
      "h264"
      "opus"
    ];
    users = {
      K900 = {
        password = {
          type = "bcrypt";
          key = "$2a$10$ibYBDqERDWFVpqMecJ8.BenJ33wx5lIbNv6zbjK.3UchdEMdwIQCO";
        };
        permissions = "op";
      };
    };
    authKeys = [
      {
        kty = "EC";
        crv = "P-256";
        alg = "ES256";
        x = "Wcz-GbjD5I0kM7wI7dR--Z-fS5XcfcZuBqmbnJcZ3Ms";
        y = "ZX0pX4tO1kCUiSMDN33wcQInsnZEbXOPd447-pNa6GA";
      }
    ];
    authPortal = "https://0upti.me";
    wildcard-user = {
      password = {
        type = "wildcard";
      };
      permissions = "present";
    };
  };
  makeGroup =
    name: displayName:
    pkgs.writeTextFile {
      name = "galene-group-${name}.json";
      text = builtins.toJSON (defaultGroupConfig // { inherit displayName; });
      destination = "/${name}.json";
    };
  allGroups = {
    "general" = "General";
    "pivo" = "Deep Rock Galactic";
    "tabletop" = "Tabletop & role playing";
  };
in
{
  services = {
    galene = {
      enable = true;

      httpAddress = "127.0.0.1";
      httpPort = 8444;
      insecure = true;

      dataDir = builtins.path {
        path = ./data;
        name = "galene-data";
      };
      groupsDir = pkgs.symlinkJoin {
        name = "galene-groups";
        paths = lib.mapAttrsToList makeGroup allGroups;
      };

      # FIXME: auth fix, remove when merged
      package = pkgs.galene.overrideAttrs (old: {
        patches = old.patches or [ ] ++ [
          (pkgs.fetchpatch {
            url = "https://github.com/K900/galene/commit/d129fd54acc078a711f7931a7f5261899aab8993.patch";
            hash = "sha256-ZijuDei0WQms2Jnt7QHwPEOMt7HekU+2YhETOQMBYcM=";
          })
        ];
      });
    };

    nginx = {
      upstreams.galene.servers."127.0.0.1:8444" = { };

      virtualHosts."chat.0upti.me".locations = {
        "/" = {
          proxyPass = "http://galene";
          proxyWebsockets = true;
        };

        "/public-groups.json" = {
          proxyPass = "http://galene";
          proxyWebsockets = true;
          extraConfig = "add_header Access-Control-Allow-Origin *;";
        };
      };
    };
  };

  networking.firewall = {
    allowedTCPPorts = [ 1194 ];
    allowedUDPPorts = [ 1194 ];
  };
}
