{
  imports = [
    ../../shared/platform/hetzner.nix
    ../../shared/server.nix

    ../../shared/applications/server/acme.nix
    ../../shared/applications/server/nginx.nix
    ../../shared/applications/server/postgres.nix

    ../../shared/applications/monitoring
    ../../shared/applications/monitoring/nginx_exporter.nix
    ../../shared/applications/monitoring/postgres_exporter.nix

    ../../shared/applications/tailscale.nix

    ./kanidm.nix

    ./fileserver.nix
    ./murmur.nix

    ./akkoma.nix
    ./grapevine
    ./lemmy.nix
    ./vaultwarden.nix

    ./headscale.nix
    ./shadowsocks.nix
    ./wireguard.nix

    ./miniflux.nix

    ./0uptime-api.nix
    ./0uptime-ui.nix
    ./chinatsu.nix

    ./factorio.nix

    ./galene

    # ../../utils/upgrade-postgres.nix
  ];

  networking = {
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
      "8.8.8.8"
    ];
    nftables.tables.grchc = {
      family = "inet";
      content = ''
        set GRCHC {
          type ipv4_addr
          elements = {
            185.224.228.0/24,
            185.224.229.0/24,
            185.224.230.0/24,
            185.224.231.0/24,
            195.209.120.0/24,
            195.209.121.0/24,
            195.209.122.0/24,
            195.209.123.0/24,
            212.192.156.0/24,
            212.192.157.0/24,
            212.192.158.0/24
          }
          flags interval
        }

        chain input {
          type filter hook input priority 0
          ip saddr @GRCHC counter drop
        }
      '';
    };
  };

  systemd.network.networks.hetzner.address = [
    "95.216.204.176/32"
    "2a01:4f9:c010:45c2::1/128"
  ];
}
