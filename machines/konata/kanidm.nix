{ config, ... }:
let
  certsDirectory = config.security.acme.certs."auth.0upti.me".directory;
in
{
  services = {
    kanidm = {
      enableServer = true;
      serverSettings = {
        bindaddress = "0.0.0.0:8443";
        ldapbindaddress = "0.0.0.0:636";

        domain = "auth.0upti.me";
        origin = "https://auth.0upti.me";

        tls_chain = "${certsDirectory}/fullchain.pem";
        tls_key = "${certsDirectory}/key.pem";
      };

      enableClient = true;
      clientSettings.uri = "https://auth.0upti.me";
    };

    nginx = {
      upstreams.kanidm.servers."127.0.0.1:8443" = { };

      virtualHosts."auth.0upti.me".locations."/" = {
        proxyPass = "https://kanidm";
        proxyWebsockets = true;
      };
    };
  };

  systemd.services.kanidm = {
    after = [ "acme-auth.0upti.me.service" ];
    serviceConfig.SupplementaryGroups = [ "acme" ];
  };

  security.acme.certs."auth.0upti.me".reloadServices = [ "kanidm.service" ];

  networking.firewall.allowedTCPPorts = [ 636 ];
}
