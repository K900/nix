{ config, pkgs, ... }:

{
  services = {
    prometheus.exporters.json = {
      enable = true;
      listenAddress = "localhost";
      port = 9007;

      configFile = (pkgs.formats.yaml { }).generate "json-exporter-config.yml" {
        modules.matrix-federation.metrics = [
          {
            name = "matrix_homeserver_federation_ok";
            path = "{.FederationOK}";
            help = "False if there's any problem with federation reported.";
            type = "value";
            value_type = "gauge";
          }
        ];
      };
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "grapevine";

        scrape_configs = [
          {
            job_name = "grapevine-federation";
            metrics_path = "/probe";
            params.module = [ "matrix-federation" ];
            relabel_configs = [
              {
                source_labels = [ "__address__" ];
                target_label = "__param_target";
              }
              {
                source_labels = [ "__address__" ];
                target_label = "instance";
              }
              {
                target_label = "__address__";
                replacement = "localhost:${toString config.services.prometheus.exporters.json.port}";
              }
            ];

            static_configs = [
              {
                targets = [ "https://federationtester.matrix.org/api/report?server_name=0upti.me" ];
                labels.matrix_instance = "0upti.me";
              }
            ];
          }
        ];
      }
    ];
  };
}
