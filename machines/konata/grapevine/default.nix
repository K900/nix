{ inputs, ... }:
{
  imports = [
    inputs.grapevine.nixosModules.default
    ./federation-monitoring.nix
    ./heisenbridge.nix
  ];

  services = {
    grapevine = {
      enable = true;
      settings = {
        listen = [
          {
            type = "tcp";
            address = "127.0.0.1";
            port = 6167;
          }
        ];
        server_name = "0upti.me";
        database.backend = "rocksdb";
        conduit_compat = true;

        server_discovery = {
          server.authority = "matrix.0upti.me:443";
          client.base_url = "https://matrix.0upti.me";
        };

        # breaks startup order funny
        federation.self_test = false;

        # FIXME: remove eventually
        serve_media_unauthenticated = true;
      };
    };

    nginx = {
      upstreams.grapevine.servers."127.0.0.1:6167" = { };

      virtualHosts = {
        "matrix.0upti.me".locations."/".proxyPass = "http://grapevine";
        "0upti.me".locations."/.well-known/matrix".proxyPass = "http://grapevine";
      };
    };
  };
}
