{
  services.heisenbridge = {
    enable = true;
    homeserver = "https://matrix.0upti.me";
    owner = "@k900:0upti.me";
    identd.enable = true;
  };

  systemd.services.heisenbridge.after = [ "grapevine.service" ];

  networking.firewall.allowedTCPPorts = [ 113 ];
}
