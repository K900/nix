{ config, lib, ... }:
let
  certsDirectory = config.security.acme.certs."0upti.me".directory;
in
{
  services = {
    murmur = {
      enable = true;

      registerName = "0upti.me";
      welcometext = "<br />Welcome to the <b>0upti.me</b> Mumble server.<br />Poke @K900 if shit goes wrong.<br />";

      bandwidth = 200000;
      users = 100;

      sslCert = "${certsDirectory}/fullchain.pem";
      sslKey = "${certsDirectory}/key.pem";

      dbus = "system";
    };

    # add redirect page to Nginx
    nginx.virtualHosts."mumble.0upti.me".locations."/".return = "301 mumble://0upti.me$request_uri";
  };

  # allow Murmur to access our certs
  systemd.services.murmur = {
    after = [ "acme-0upti.me.service" ];
    serviceConfig.Group = lib.mkForce "acme";
  };

  security.acme.certs."0upti.me".reloadServices = [ "murmur.service" ];

  networking.firewall = {
    allowedTCPPorts = [ 64738 ];
    allowedUDPPorts = [ 64738 ];
  };
}
