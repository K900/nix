{ inputs, ... }:
{
  imports = [ inputs.chinatsu.nixosModules.default ];

  age.secrets.chinatsu.file = ../../secrets/chinatsu.age;

  services = {
    chinatsu = {
      enable = true;
      configFile = "/run/agenix/chinatsu";
    };

    postgresql = {
      authentication = "local chinatsu chinatsu peer map=allow-root-chinatsu";
      identMap = "allow-root-chinatsu root chinatsu";

      ensureDatabases = [ "chinatsu" ];
      ensureUsers = [
        {
          name = "chinatsu";
          ensureDBOwnership = true;
        }
      ];
    };
  };
}
