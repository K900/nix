{
  config,
  lib,
  pkgs,
  ...
}:
# FIXME: upstream maybe?
let
  ipv4 = "10.10.0.1";
  ipv6 = "fdb0:6e09:474b::1";

  aipv4 = "10.11.0.1";
  aipv6 = "fdb0:6e09:474c::1";
in
{
  boot = {
    extraModulePackages = [ config.boot.kernelPackages.amneziawg ];
    kernelModules = [ "amneziawg" ];
  };

  networking = {
    nat = {
      enable = true;
      enableIPv6 = true;
      internalIPs = [
        "${ipv4}/16"
        "${aipv4}/16"
      ];
      internalIPv6s = [
        "${ipv6}/48"
        "${aipv6}/48"
      ];
    };

    firewall = {
      allowedUDPPorts = [
        51820
        51822
      ];
      interfaces.wg0 = {
        allowedTCPPorts = [ 53 ];
        allowedUDPPorts = [ 53 ];
      };
      interfaces.awg0 = {
        allowedTCPPorts = [ 53 ];
        allowedUDPPorts = [ 53 ];
      };
    };
  };

  systemd = {
    services = {
      amneziawg = {
        wantedBy = [ "network.target" ];

        path = [
          pkgs.iproute2
          pkgs.amneziawg-tools
        ];

        script = ''
          ip link add awg0 type amneziawg
          awg set awg0 \
            listen-port 51822 \
            private-key /run/agenix/wireguard \
            jc 5 \
            jmin 40 \
            jmax 70 \
            s1 10 \
            s2 5 \
            h1 1548060210 \
            h2 1423311709 \
            h3 1803398043 \
            h4 1351438416 
        '';
      };
    };

    network = {
      netdevs.wg0 = {
        netdevConfig = {
          Name = "wg0";
          Kind = "wireguard";
        };
        wireguardConfig = {
          ListenPort = 51820;
          PrivateKeyFile = "/run/agenix/wireguard";
        };
      };
      networks = {
        wg0 = {
          name = "wg0";
          address = [
            "${ipv4}/16"
            "${ipv6}/48"
          ];
        };
        awg0 = {
          name = "awg0";
          address = [
            "${aipv4}/16"
            "${aipv6}/48"
          ];
        };
      };

      wait-online.ignoredInterfaces = [
        "wg0"
        "awg0"
      ];
    };
  };

  age.secrets.wireguard = {
    file = ../../secrets/wireguard.age;
    owner = "systemd-network";
  };

  services = {
    dnsmasq = {
      enable = true;
      resolveLocalQueries = false;

      settings = {
        # proxy to local resolved
        server = [ "127.0.0.53" ];
        interface = [
          "wg0"
          "awg0"
        ];
        bind-interfaces = true;
        cache-size = 5000;
        host-record = "server.vpn.0upti.me,${ipv4},${ipv6}";
      };
    };

    nginx.virtualHosts."server.vpn.0upti.me" = {
      listen = lib.flatten (
        builtins.map
          (addr: [
            {
              inherit addr;
              port = 80;
            }
            {
              inherit addr;
              port = 443;
              ssl = true;
            }
          ])
          [
            ipv4
            aipv4
            "[${ipv6}]"
            "[${aipv6}]"
          ]
      );

      locations."/check" = {
        return = "200";
        extraConfig = "add_header Access-Control-Allow-Origin *;";
      };
    };
  };

  environment = {
    systemPackages = [
      pkgs.wireguard-tools
      pkgs.amneziawg-tools
    ];
  };
}
