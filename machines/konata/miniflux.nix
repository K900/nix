{
  age.secrets.miniflux_client_secret.file = ../../secrets/miniflux_client_secret.age;

  services = {
    miniflux = {
      enable = true;
      createDatabaseLocally = true;
      config = {
        LISTEN_ADDR = "localhost:7373";
        BASE_URL = "https://rss.0upti.me/";
        CREATE_ADMIN = 0;
        DISABLE_LOCAL_AUTH = 1;
        METRICS_COLLECTOR = 1;
        OAUTH2_PROVIDER = "oidc";
        OAUTH2_CLIENT_ID = "miniflux";
        OAUTH2_CLIENT_SECRET_FILE = "%d/client_secret";
        OAUTH2_REDIRECT_URL = "https://rss.0upti.me/oauth2/oidc/callback";
        OAUTH2_OIDC_DISCOVERY_ENDPOINT = "https://auth.0upti.me/oauth2/openid/miniflux";
        OAUTH2_USER_CREATION = 1;
      };
    };

    nginx = {
      upstreams.miniflux.servers."127.0.0.1:7373" = { };
      virtualHosts."rss.0upti.me".locations."/".proxyPass = "http://miniflux";
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "miniflux";
        scrape_configs = [
          {
            job_name = "miniflux";
            static_configs = [ { targets = [ "localhost:7373" ]; } ];
          }
        ];
      }
    ];
  };

  systemd.services.miniflux.serviceConfig.LoadCredential =
    "client_secret:/run/agenix/miniflux_client_secret";

  # FIXME?: broken on v6, force v4
  networking.hosts = {
    "::" = [
      "alphyna.org"
      "maremir.org"
    ];
    "46.36.222.38" = [
      "alphyna.org"
      "maremir.org"
    ];
  };
}
