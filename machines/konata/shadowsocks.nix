{
  age.secrets.shadowsocks.file = ../../secrets/shadowsocks.age;

  services.shadowsocks = {
    enable = true;
    passwordFile = "/run/agenix/shadowsocks";
  };

  networking.firewall = {
    allowedTCPPorts = [ 8388 ];
    allowedUDPPorts = [ 8388 ];
  };
}
