{ inputs, pkgs, ... }:
{
  services.nginx.virtualHosts."0upti.me" = {
    root = inputs.zero-uptime-ui.packages.${pkgs.stdenv.hostPlatform.system}.default;

    locations = {
      "/" = {
        tryFiles = "$uri $uri/ /index.html";
        extraConfig = ''
          add_header Pragma public;
          add_header Cache-Control "public, no-cache, must-revalidate";
        '';
      };
      "~* \.(css|js|jpg|png)$" = {
        extraConfig = ''
          expires 30d;
          add_header Pragma public;
          add_header Cache-Control "public";
        '';
      };
    };
  };
}
