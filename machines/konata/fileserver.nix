{
  services.nginx.virtualHosts."static.0upti.me" = {
    root = "/srv/http";
    extraConfig = "autoindex on;";
  };
}
