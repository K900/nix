{ lib, pkgs, ... }:
let
  unfoldAttrs =
    { name, value }:
    map (v: {
      inherit name;
      value = v;
    }) value;
  renderService =
    { name, value }:
    {
      name = "${value}.ts.0upti.me";
      type = "A";
      value = name;
    };
  mapServices =
    x:
    lib.pipe x [
      lib.attrsToList
      (map unfoldAttrs)
      lib.flatten
      (map renderService)
    ];
in
{
  age.secrets.headscale_client_secret = {
    file = ../../secrets/headscale_client_secret.age;
    owner = "headscale";
  };

  services = {
    headscale = {
      enable = true;

      settings = {
        server_url = "https://ts.0upti.me/";
        listen_addr = "127.0.0.1:8085";
        metrics_listen_addr = "0.0.0.0:9092";

        prefixes = {
          v6 = "fd7a:115c:a1e0::/48";
          v4 = "100.64.0.0/10";
        };

        database = {
          type = "postgres";
          postgres = {
            host = "/run/postgresql";
            name = "headscale";
            user = "headscale";
          };
        };

        dns = {
          base_domain = "nodes.ts.0upti.me";

          nameservers.global = [
            "1.1.1.1"
            "1.0.0.1"
            "2606:4700:4700::1111"
            "2606:4700:4700::1001"
          ];

          search_domains = [ "ts.0upti.me" ];

          extra_records = mapServices {
            # morgana
            "100.64.0.3" = [
              "morgana-auth"
              "fluidd"
              "go2rtc"
            ];

            # qb
            "100.64.0.4" = [
              "qb-auth"

              # Monitoring
              "grafana"
              "loki"
              "mimir"

              # Home stuff
              "hass"
              "succ"
            ];

            # index
            "100.64.0.14" = [
              "index-auth"

              # Media
              "jellyfin"

              # Ahoy
              "bt"
              "prowlarr"
              "sonarr"
              "radarr"
              "lidarr"
              "bazarr"
              "bitmagnet"

              # Doc storage
              "paperless"

              # Sync
              "syncthing"
            ];
          };
        };

        oidc = {
          only_start_if_oidc_is_available = true;
          issuer = "https://auth.0upti.me/oauth2/openid/headscale";
          client_id = "headscale";
          client_secret_path = "/run/agenix/headscale_client_secret";
          scope = [
            "openid"
            "profile"
            "email"
          ];
          strip_email_domain = true;
          pkce.enabled = true;
        };

        derp.server = {
          enabled = true;
          region_code = "fi";
          region_name = "0upti.me - Finland";
          stun_listen_addr = "0.0.0.0:3478";
          ipv4 = "95.216.204.176";
          ipv6 = "2a01:4f9:c010:45c2::1";
        };
      };
    };

    tailscale.enable = true;

    postgresql = {
      ensureDatabases = [ "headscale" ];
      ensureUsers = [
        {
          name = "headscale";
          ensureDBOwnership = true;
        }
      ];
    };

    nginx = {
      upstreams.headscale.servers."127.0.0.1:8085" = { };

      virtualHosts."ts.0upti.me".locations."/" = {
        proxyPass = "http://headscale";
        proxyWebsockets = true;
      };
    };
  };

  systemd.services.headscale.after = [
    "kanidm.service"
    "nginx.service"
  ];

  networking.firewall = {
    allowedUDPPorts = [ 3478 ];
    interfaces.tailscale0.allowedTCPPorts = [ 9092 ];
  };

  environment.systemPackages = [ pkgs.headscale ];
}
