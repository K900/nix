{
  services = {
    lemmy = {
      enable = true;

      database.createLocally = true;
      nginx.enable = true;

      settings.hostname = "lemmy.0upti.me";
    };
  };
}
