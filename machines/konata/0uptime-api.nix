{ inputs, ... }:
{
  imports = [ inputs.zero-uptime-api.nixosModules.default ];

  age.secrets.galene-token-key = {
    file = ../../secrets/galene-token-key.age;
    owner = "zero-uptime-api";
  };

  services = {
    zero-uptime-api = {
      enable = true;
      listenAddress = "127.0.0.1:8000";
      galeneKeyFile = "/run/agenix/galene-token-key";
    };

    zero-uptime-exporter = {
      enable = true;
      listenAddress = "127.0.0.1:9006";
      databaseUrl = "postgres:///chinatsu?user=chinatsu&host=/run/postgresql";
    };

    nginx = {
      upstreams.api.servers."127.0.0.1:8000" = { };

      virtualHosts."api.0upti.me" = {
        locations."/".proxyPass = "http://api";

        extraConfig = ''
          proxy_buffer_size 256k;
          proxy_buffers 8 256k;
        '';
      };
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "zero-uptime-api";
        scrape_configs = [
          {
            job_name = "zero-uptime-api";
            static_configs = [ { targets = [ "localhost:9006" ]; } ];
          }
        ];
      }
    ];
  };
}
