{ pkgs, ... }:
{
  age.secrets = {
    akkoma_secret_key_base = {
      file = ../../secrets/akkoma/secret_key_base.age;
      owner = "akkoma";
    };

    akkoma_signing_salt = {
      file = ../../secrets/akkoma/signing_salt.age;
      owner = "akkoma";
    };

    akkoma_live_view_signing_salt = {
      file = ../../secrets/akkoma/live_view_signing_salt.age;
      owner = "akkoma";
    };

    akkoma_vapid_private_key = {
      file = ../../secrets/akkoma/vapid_private_key.age;
      owner = "akkoma";
    };

    akkoma_vapid_public_key = {
      file = ../../secrets/akkoma/vapid_public_key.age;
      owner = "akkoma";
    };

    akkoma_default_signer = {
      file = ../../secrets/akkoma/default_signer.age;
      owner = "akkoma";
    };
  };

  services = {
    akkoma = {
      enable = true;
      initSecrets = false;

      extraStatic = {
        "emoji/blobs.gg" = pkgs.akkoma-emoji.blobs_gg;
        "static/terms-of-service.html" = pkgs.writeTextFile {
          name = "tos.html";
          text = ''
            <p>The first rule of Fight Club is: you do not talk about Fight Club.</p>
            <p>The second rule of Fight Club is: you <strong>do not</strong> talk about Fight Club!</p>

            <p>Jokes aside, this is a personal instance, there are no rules, this message is just here to replace the default placeholder.</p>
          '';
        };
      };

      config = {
        ":pleroma" = {
          ":instance" = {
            name = "Zero Uptime";
            description = "K900's personal server";
            email = "me@0upti.me";
            notify_email = "me@0upti.me";
            limit = 5000;
            registrations_open = false;
            public = false;
          };

          "Pleroma.Web.Endpoint" = {
            url = {
              host = "toot.0upti.me";
              scheme = "https";
              port = 443;
            };
            http = {
              ip = "127.0.0.1";
              port = 4000;
            };

            secret_key_base._secret = "/run/agenix/akkoma_secret_key_base";
            signing_salt._secret = "/run/agenix/akkoma_signing_salt";
            live_view.signing_salt._secret = "/run/agenix/akkoma_live_view_signing_salt";
          };

          "Pleroma.Upload".base_url = "https://toot.0upti.me/media/";

          ":http_security".sts = true;

          ":media_proxy".enabled = false;
          ":shout".enabled = false;

          ":restrict_unauthenticated" = {
            timelines = {
              local = false;
              federated = true;
            };
            profiles = {
              local = false;
              remote = false;
            };
            activities = {
              local = false;
              remote = false;
            };
          };
        };

        ":web_push_encryption".":vapid_details" = {
          private_key._secret = "/run/agenix/akkoma_vapid_private_key";
          public_key._secret = "/run/agenix/akkoma_vapid_public_key";
        };

        ":joken".":default_signer"._secret = "/run/agenix/akkoma_default_signer";
      };
    };

    nginx = {
      upstreams.akkoma.servers."127.0.0.1:4000" = { };

      virtualHosts."toot.0upti.me".locations."/" = {
        proxyPass = "http://akkoma";
        proxyWebsockets = true;

        extraConfig = ''
          client_max_body_size 16m;
        '';
      };
    };
  };

  # Has some weird memory leak, prevent it from killing the machine.
  systemd.services.akkoma.serviceConfig.MemoryMax = "1G";
}
