{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    ../shared/desktop-base.nix
    "${inputs.jovian}/modules"
    inputs.aagl.nixosModules.default
  ];

  # avoid very slow rebuilds
  nixpkgs.overlays = [
    (final: _: {
      electron_33 = final.electron_33-bin;
    })
  ];

  jovian = {
    steam = {
      enable = true;
      autoStart = true;
      user = "k900";
      desktopSession = "plasma";
      updater.splash = "jovian";
    };
    decky-loader = {
      enable = true;
      package = pkgs.decky-loader-prerelease;
    };
    devices.steamdeck = {
      enable = true;
      autoUpdate = true;
      enableFwupdBiosUpdates = false;
      enableGyroDsuService = true;
    };
  };

  services = {
    displayManager.sddm.enable = lib.mkForce false;
    flatpak.enable = true;
  };

  programs = {
    anime-game-launcher.enable = true;
    honkers-railway-launcher.enable = true;
    sleepy-launcher.enable = true;

    # used by Decky PowerTools
    nix-ld = {
      enable = true;
      libraries = [ pkgs.pciutils ];
    };
  };

  boot.loader.bootis = {
    extraEntries."SteamOS" = {
      volume = "a869a310-14c3-f048-9ebb-5a20102478ac";
      loader = "/EFI/steamos/steamcl.efi";
      icon = "/EFI/refind/themes/regular/icons/128-48/os_steam.png";
      graphics = "on";
    };

    extraConfig.resolution = lib.mkForce "1280 800";

    refind.theme.regular = {
      size = "s";
      font.size = 18;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "btrfs";
      options = [ "compress=zstd" ];
    };

    "/boot" = {
      device = "/dev/disk/by-label/NIXOS-ESP";
      fsType = "vfat";
    };

    "/mnt/games" = {
      device = "/dev/disk/by-label/games";
      fsType = "btrfs";
      options = [ "compress=zstd" ];
    };
  };

  environment.systemPackages = [
    pkgs.steamdeck-firmware

    pkgs.ryubing
    pkgs.steam-rom-manager

    pkgs.heroic
    pkgs.protontricks

    pkgs.jellyfin-media-player

    pkgs.maliit-framework
    pkgs.maliit-keyboard
  ];

  powerManagement.cpuFreqGovernor = "schedutil";
}
