{ pkgs, ... }:
let
  mapNtfs = import ../utils/map-ntfs.nix pkgs;
in
{
  imports = [
    ../shared/desktop.nix
    ../shared/homenet-builders.nix
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    initrd = {
      kernelModules = [ "amdgpu" ];
      availableKernelModules = [
        "nvme"
        "xhci_pci"
        "ahci"
        "usbhid"
        "usb_storage"
        "sd_mod"
      ];
    };
    kernelModules = [ "kvm-amd" ];
    kernelParams = [ "amd_pstate=active" ];

    loader.bootis = {
      extraEntries."Windows 11".loader = "/EFI/Microsoft/Boot/bootmgfw.efi";

      refind.theme.regular = {
        size = "m";
        font.size = 32;
      };
    };
  };

  hardware = {
    cpu.amd.updateMicrocode = true;
    enableRedistributableFirmware = true;
  };

  services = {
    hardware.openrgb.enable = true;
    # udev rules for keyboard
    udev.packages = [
      pkgs.vial
      pkgs.qmk-udev-rules
    ];
  };

  fileSystems =
    {
      "/" = {
        device = "/dev/disk/by-label/nixos_root";
        fsType = "btrfs";
        options = [ "compress=zstd" ];
      };

      "/boot" = {
        device = "/dev/disk/by-label/boot";
        fsType = "vfat";
      };
    }
    // mapNtfs {
      "c" = "windows_root";
      "d" = "work";
      "e" = "backups";
      "f" = "media";
      "g" = "games1";
      "h" = "games2";
      "i" = "games3";
      "j" = "games4";
    };

  environment.systemPackages = [
    pkgs.vial
    pkgs.prusa-slicer
  ];

  programs.corectrl = {
    enable = true;
    gpuOverclock = {
      ppfeaturemask = "0xffffffff";
      enable = true;
    };
  };
}
