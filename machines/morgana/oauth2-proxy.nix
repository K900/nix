{
  imports = [ ../../shared/applications/server/oauth2-proxy-base.nix ];

  age.secrets.oauth2_proxy_secrets = {
    file = ../../secrets/morgana_oauth2_proxy_secrets.age;
    owner = "oauth2-proxy";
  };

  services.oauth2-proxy = {
    oidcIssuerUrl = "https://auth.0upti.me/oauth2/openid/morgana";
    clientID = "morgana";

    nginx = {
      domain = "morgana-auth.ts.0upti.me";
      virtualHosts."go2rtc.ts.0upti.me".allowed_groups = [ "klipper_users@auth.0upti.me" ];
    };
  };
}
