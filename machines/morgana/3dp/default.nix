{ lib, pkgs, ... }:
{
  age.secrets.moonraker = {
    file = ../../../secrets/moonraker.age;
    owner = "moonraker";
  };

  imports = [ ./vyper.nix ];

  services = {
    klipper = {
      enable = true;

      user = "moonraker";
      group = "moonraker";

      settings = {
        # these need to be calibrated per-printer
        extruder = {
          control = "pid";
          pid_kp = 28.798;
          pid_ki = 1.362;
          pid_kd = 152.269;
          rotation_distance = 22.455;
          pressure_advance = 0.45;
        };

        heater_bed = {
          control = "pid";
          pid_kp = 57.411;
          pid_ki = 0.667;
          pid_kd = 1235.764;
        };

        probe.z_offset = -0.295;

        "bed_mesh default" = {
          version = 1;
          points = [
            "-3.282500, -3.167500, -3.077500, -3.007500, -2.937500, -2.880000, -2.895000, -2.695000, -2.737500, -2.605000"
            "-3.247500, -3.232500, -3.137500, -3.057500, -2.937500, -2.947500, -2.795000, -2.787500, -2.757500, -2.582500"
            "-3.395000, -3.212500, -3.110000, -2.990000, -2.947500, -2.957500, -2.877500, -2.737500, -2.662500, -2.527500"
            "-3.377500, -3.165000, -3.362500, -3.107500, -3.032500, -3.047500, -2.892500, -2.770000, -2.757500, -2.560000"
            "-3.360000, -3.237500, -3.252500, -3.147500, -3.092500, -3.132500, -2.957500, -2.797500, -2.747500, -2.682500"
            "-3.315000, -3.335000, -3.180000, -3.147500, -3.092500, -3.140000, -3.007500, -2.822500, -2.737500, -2.605000"
            "-3.350000, -3.310000, -3.277500, -3.142500, -3.085000, -3.092500, -3.067500, -2.850000, -2.752500, -2.637500"
            "-3.370000, -3.462500, -3.325000, -3.142500, -3.122500, -3.040000, -2.947500, -2.782500, -2.815000, -2.687500"
            "-3.385000, -3.335000, -3.407500, -3.080000, -3.100000, -3.020000, -3.195000, -2.845000, -2.782500, -2.635000"
            "-3.367500, -3.390000, -3.200000, -3.115000, -3.337500, -3.010000, -2.900000, -2.925000, -2.740000, -2.615000"
          ];
          x_count = 10;
          y_count = 10;
          mesh_x_pps = 2;
          mesh_y_pps = 2;
          algo = "bicubic";
          tension = 0.2;
          min_x = 15.0;
          max_x = 230.0;
          min_y = 15.0;
          max_y = 230.0;
        };

        virtual_sdcard.path = "/var/lib/moonraker/gcodes";
      };
    };

    moonraker = {
      enable = true;
      allowSystemControl = true;
      analysis.enable = true;
      address = "0.0.0.0"; # FIXME: breaks klipper-estimator detection upstream

      settings = {
        octoprint_compat = { };
        history = { };
        authorization.default_source = "ldap";

        ldap = {
          ldap_host = "auth.0upti.me";
          ldap_port = 636;
          ldap_secure = true;
          base_dn = "dc=auth,dc=0upti,dc=me";
          group_dn = "spn=klipper_users@auth.0upti.me,dc=auth,dc=0upti,dc=me";
        };

        file_manager.queue_gcode_uploads = true;
        job_queue.load_on_startup = true;

        analysis = {
          enable_auto_analysis = true;
          auto_dump_default_config = true;
        };

        "webcam Camera" = rec {
          service = "iframe";
          stream_url = "https://go2rtc.ts.0upti.me/stream.html?src=printer";
          snapshot_url = stream_url;
          aspect_ratio = "16:9";
        };

        "power Plug" = {
          type = "homeassistant";
          address = "hass.ts.0upti.me";
          protocol = "https";
          port = 443;
          device = "switch.printer_plug";
          token = "{secrets.home_assistant.token}";

          off_when_shutdown = true;
          on_when_job_queued = true;
          locked_while_printing = true;
          restart_klipper_when_powered = true;
        };

        "notifier print_start" = {
          url = "{secrets.telegram.url}";
          events = "started";
          body = "Started printing: {event_args[1].filename}";
        };

        "notifier print_complete" = {
          url = "{secrets.telegram.url}";
          events = "complete";
          body = "Done printing: {event_args[1].filename}";
        };

        "notifier print_error" = {
          url = "{secrets.telegram.url}";
          events = "error";
          body = "Print error: {event_args[1].message}";
        };
      };
    };

    fluidd = {
      enable = true;
      hostName = "fluidd.ts.0upti.me";
    };

    go2rtc = {
      enable = true;
      settings = {
        streams.printer = "ffmpeg:device?video=/dev/v4l/by-id/usb-ANYKA_FHD_Camera_12345-video-index0&input_format=h264";
        ffmpeg.bin = "${lib.getBin pkgs.ffmpeg_6-full}/bin/ffmpeg";
        webrtc.candidates = [ "100.64.0.11:8555" ];
      };
    };

    nginx = {
      clientMaxBodySize = "500M";

      virtualHosts."go2rtc.ts.0upti.me".locations."/" = {
        proxyPass = "http://localhost:1984";
        proxyWebsockets = true;
      };
    };
  };

  systemd.tmpfiles.settings.moonraker."/var/lib/moonraker/moonraker.secrets".L.argument =
    "/run/agenix/moonraker";

  # allow webrtc
  networking.firewall = {
    allowedTCPPorts = [ 8555 ];
    allowedUDPPorts = [ 8555 ];
  };

  # mostly unnecessary but sure
  security.polkit.enable = true;
}
