{ lib, ... }:
# helper hack to not write long macros as a list-of-strings
let
  macro = lib.strings.splitString "\n";
in
{
  # Generic settings for Anycubic Vyper on RPi, mostly lifted from
  # - https://github.com/cryd-s/Vyper_extended/blob/main/printer.cfg
  # - https://github.com/Klipper3d/klipper/blob/master/config/printer-anycubic-vyper-2021.cfg
  #
  # These should generally not need to be tweaked.
  services.klipper = {
    firmwares.mcu = {
      # enable = true;
      configFile = ./klipper.mk;
    };

    settings = {
      mcu = {
        serial = "/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0";
        restart_method = "command";
      };

      printer = {
        kinematics = "cartesian";
        max_velocity = 300;
        max_accel = 3000;
        minimum_cruise_ratio = 0.1;
        max_z_velocity = 5;
        max_z_accel = 100;
      };

      stepper_x = {
        step_pin = "PC2";
        dir_pin = "PB9";
        enable_pin = "!PC3";
        rotation_distance = 40;
        microsteps = 16;
        endstop_pin = "!PA7";
        position_min = -3;
        position_endstop = -3;
        position_max = 245;
        homing_speed = 30;
      };

      stepper_y = {
        step_pin = "PB8";
        dir_pin = "PB7";
        enable_pin = "!PC13";
        rotation_distance = 32;
        microsteps = 16;
        endstop_pin = "!PC5";
        position_min = -17;
        position_endstop = -17;
        position_max = 245;
        homing_speed = 30;
      };

      stepper_z = {
        step_pin = "PB6";
        dir_pin = "!PB5";
        enable_pin = "!PC14";
        rotation_distance = 8;
        microsteps = 16;
        endstop_pin = "PB2";
        position_endstop = 0;
        position_max = 260;
        position_min = -5;
        homing_speed = 5;
      };

      stepper_z1 = {
        step_pin = "PC0";
        dir_pin = "!PC1";
        enable_pin = "!PC15";
        rotation_distance = 8;
        microsteps = 16;
        endstop_pin = "PC6";
      };

      safe_z_home = {
        home_xy_position = "-3, -17";
        z_hop = "10";
      };

      extruder = {
        step_pin = "PB4";
        dir_pin = "!PB3";
        enable_pin = "!PA15";
        microsteps = 16;
        rotation_distance = lib.mkDefault 22.765;
        gear_ratio = "50:17";
        nozzle_diameter = 0.4;
        filament_diameter = 1.75;
        heater_pin = "PA1";
        sensor_type = "ATC Semitec 104GT-2";
        sensor_pin = "PC4";
        min_temp = 0;
        max_temp = 260;
      };

      "heater_fan extruder_fan".pin = "PB1";

      bed_mesh = {
        mesh_min = "15, 15";
        mesh_max = "230, 230";
        probe_count = "6, 6";
        speed = 40;
      };

      heater_bed = {
        heater_pin = "PA4";
        sensor_type = "EPCOS 100K B57560G104F";
        sensor_pin = "PB0";
        min_temp = "0";
        max_temp = "110";
      };

      fan.pin = "PA0";

      probe = {
        pin = "!PB12";
        activate_gcode = [ "_PROBE_RESET" ];
      };

      "output_pin LED".pin = "PA13";
      "output_pin PROBE_RESET".pin = "PB13";
      "output_pin BEEP" = {
        pin = "PB15";
        pwm = true;
        cycle_time = 1.0e-2;
      };

      # "filament_switch_sensor runout" = {
      #   switch_pin = "PA5";
      #   pause_on_runout = true;
      # };

      "temperature_sensor Host" = {
        sensor_type = "temperature_host";
        min_temp = 0;
        max_temp = 100;
      };

      "temperature_fan Mainboard" = {
        pin = "PA14";
        sensor_type = "temperature_mcu";
        max_power = 1;
        shutdown_speed = 1;
        hardware_pwm = false;
        kick_start_time = 0.5;
        off_below = 0.4;
        max_temp = 100;
        min_temp = 20;
        target_temp = 70;
        max_speed = 1;
        min_speed = 0.3;
        control = "pid";
        pid_Kp = 1;
        pid_Ki = 1;
        pid_Kd = 1;
      };

      pause_resume = { };
      display_status = { };

      # Some macros. G-Code reference: https://www.klipper3d.org/G-Codes.html
      # helper macro to reset the probe pin
      "gcode_macro _PROBE_RESET" = {
        description = "(internal) Reset bed probe";
        gcode = macro ''
          SET_PIN PIN=PROBE_RESET VALUE=0
          G4 P300 # sleep 300ms
          SET_PIN PIN=PROBE_RESET VALUE=1
          G4 P100 # sleep 100ms
        '';
      };

      "idle_timeout" = {
        gcode = macro ''
          TURN_OFF_HEATERS # disable all heaters
          M84 # disable all motors
          M106 S0 # disable fan

          M117 "Idle poweroff"
          M112 # power off
        '';
        timeout = 300;
      };

      # helper macros to prepare/end prints, set these up in your slicer
      "gcode_macro _START_PRINT" = {
        description = "(internal) Start a new print";
        gcode = macro ''
          {% set BED_TEMP = params.T_BED|default(60)|float %}
          {% set EXTRUDER_TEMP = params.T_EXTRUDER|default(200)|float %}

          # load default bed level calibration
          # see: https://github.com/Klipper3d/klipper/issues/5983
          BED_MESH_PROFILE LOAD=default

          # start heating bed + nozzle
          M140 S{BED_TEMP}
          M104 S{EXTRUDER_TEMP}

          G90 # switch to absolute coordinates
          G28 # go to origin

          # wait for heating to be done
          M190 S{BED_TEMP}
          M109 S{EXTRUDER_TEMP}

          G1 X10 Y5 Z0.2 F1500 # move to starting prime location
          G92 E0 # reset extruder
          G1 X80 E10 F600 # move to end prime location while extruding
          G1 X10 F5000 # do a quick wipe
        '';
      };

      "gcode_macro _END_PRINT" = {
        description = "(internal) End the current print";
        gcode = macro ''
          M140 S0 # disable bed heater
          M104 S0 # disable nozzle heater
          M106 S0 # disable fan

          G91 # switch to relative coordinates
          G1 X-2 Y-2 E-3 F300 # move away a bit and retract
          G1 Z10 F3000 # move up

          G90 # switch to absolute coordinates
          G1 X0 Y200 F2200 # move the bed to the front
          M18 # turn motors off
        '';
      };

      # beep. boop.
      "gcode_macro M300" = {
        description = "Beep.";
        gcode = macro ''
          {% set S = params.S|default(1000)|int %} # frequency, Hz
          {% set P = params.P|default(100)|int %} # duration, ms
          SET_PIN PIN=BEEP VALUE=0.85 CYCLE_TIME={ 1.0/S if S > 0 else 1 } # beep
          G4 P{P} # sleep for duration
          SET_PIN PIN=BEEP VALUE=0
        '';
      };

      # Helper macros for fluidd.
      # copied from https://docs.fluidd.xyz/configuration/initial_setup
      "gcode_macro PAUSE" = {
        description = "Pause the actual running print";
        rename_existing = "PAUSE_BASE";
        variable_extrude = 5;
        gcode = macro ''
          {% set E = printer['gcode_macro PAUSE'].extrude|float %}
          {% set x_park = printer.toolhead.axis_maximum.x|float - 5.0 %}
          {% set y_park = printer.toolhead.axis_maximum.y|float - 5.0 %}

          {% set max_z = printer.toolhead.axis_maximum.z|float %}
          {% set act_z = printer.toolhead.position.z|float %}

          {% if act_z < (max_z - 2.0) %}
              {% set z_safe = 2.0 %}
          {% else %}
              {% set z_safe = max_z - act_z %}
          {% endif %}
          PAUSE_BASE
          G91  # switch to relative coordinates
          {% if printer.extruder.can_extrude|lower == 'true' %}
            G1 E-{e} F2100  # retract a bit
          {% else %}
            {action_respond_info('Extruder not hot enough')}
          {% endif %}
          {% if 'xyz' in printer.toolhead.homed_axes %}
            G1 Z{z_safe}  # move up to safer position
            G90  # switch to absolute coordinates
            G1 X{x_park} Y{y_park} F6000  # go home
          {% else %}
            {action_respond_info('Printer not homed')}
          {% endif %}
        '';
      };

      "gcode_macro RESUME" = {
        description = "Resume the actual running print";
        rename_existing = "RESUME_BASE";
        gcode = macro ''
          {% set E = printer['gcode_macro PAUSE'].extrude|float %}
          {% if 'VELOCITY' in params|upper %}
            {% set get_params = ('VELOCITY=' + params.VELOCITY)  %}
          {% else %}
            {% set get_params = "" %}
          {% endif %}
          {% if printer.extruder.can_extrude|lower == 'true' %}
            G91  # switch to relative coordinates
            G1 E{E} F2100  # extrude a bit
          {% else %}
            {action_respond_info('Extruder not hot enough')}
          {% endif %}
          RESUME_BASE {get_params}
        '';
      };

      "gcode_macro CANCEL_PRINT" = {
        description = "Cancel the actual running print";
        rename_existing = "CANCEL_PRINT_BASE";
        gcode = macro ''
          TURN_OFF_HEATERS
          CANCEL_PRINT_BASE
        '';
      };
    };
  };
}
