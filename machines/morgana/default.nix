{
  imports = [
    ../../shared/platform/orangepi3b.nix
    ../../shared/server.nix

    ../../shared/applications/server/acme.nix
    ../../shared/applications/server/nginx.nix

    ../../shared/applications/monitoring
    ../../shared/applications/monitoring/nginx_exporter.nix

    ../../shared/applications/tailscale.nix

    ./3dp
    ./oauth2-proxy.nix
  ];

  networking = {
    useDHCP = false;
    firewall.allowedUDPPorts = [ 5353 ]; # mDNS
  };

  systemd.network = {
    enable = true;

    networks.wired = {
      name = "en*";
      DHCP = "yes";
      domains = [ "home" ];
      networkConfig.MulticastDNS = true;
    };
  };

  hardware.enableRedistributableFirmware = true;

  powerManagement.cpuFreqGovernor = "schedutil";
}
