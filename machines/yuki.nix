{ pkgs, ... }:
let
  mapNtfs = import ../utils/map-ntfs.nix pkgs;
in
{
  imports = [ ../shared/desktop.nix ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    kernelModules = [ "kvm-intel" ];
    blacklistedKernelModules = [ "nouveau" ];
    initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "ahci"
      "usbhid"
      "usb_storage"
      "sd_mod"
    ];

    loader.bootis = {
      extraEntries."Windows 11".loader = "/EFI/Microsoft/Boot/bootmgfw.efi";

      refind.theme.regular = {
        size = "m";
        font.size = 28;
      };
    };
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
    enableRedistributableFirmware = true;
  };

  # disable dGPU, see https://github.com/geminis3/envycontrol/blob/main/envycontrol.py#L28
  services.udev.extraRules = ''
    # Remove NVIDIA USB xHCI Host Controller devices, if present
    ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c0330", ATTR{power/control}="auto", ATTR{remove}="1"
    # Remove NVIDIA USB Type-C UCSI devices, if present
    ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c8000", ATTR{power/control}="auto", ATTR{remove}="1"
    # Remove NVIDIA Audio devices, if present
    ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x040300", ATTR{power/control}="auto", ATTR{remove}="1"
    # Remove NVIDIA VGA/3D controller devices
    ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x03[0-9]*", ATTR{power/control}="auto", ATTR{remove}="1"
  '';

  fileSystems =
    {
      "/" = {
        device = "/dev/disk/by-label/nixos_root";
        fsType = "btrfs";
        options = [ "compress=zstd" ];
      };

      "/boot" = {
        device = "/dev/disk/by-label/boot";
        fsType = "vfat";
      };
    }
    // mapNtfs {
      "c" = "windows_root";
      "d" = "data1";
      "e" = "data2";
    };

  powerManagement.cpuFreqGovernor = "powersave";
}
