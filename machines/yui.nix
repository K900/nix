{ pkgs, ... }:
{
  imports = [
    ../shared/platform/rpi4.nix
    ../shared/server.nix

    ../shared/applications/server/acme.nix
    ../shared/applications/server/nginx.nix

    ../shared/applications/monitoring
    ../shared/applications/monitoring/nginx_exporter.nix

    ../shared/applications/tailscale.nix
  ];

  networking = {
    useDHCP = false;
    wireless.iwd = {
      enable = true;
      settings.General.Country = "RU";
    };
  };

  systemd = {
    network = {
      enable = true;
      wait-online.anyInterface = true;

      networks = {
        wired = {
          name = "end0";
          DHCP = "yes";
          domains = [ "home" ];
        };

        wireless = {
          name = "wlan0";
          DHCP = "yes";
          domains = [ "home" ];
        };
      };
    };

    # if we're stuck, it's probably OOM
    watchdog.runtimeTime = "15s";
  };

  environment.systemPackages = with pkgs; [
    iw
    iotop
  ];

  # make console font readable on 27" 1440p
  console = {
    font = "ter-i32b";
    packages = [ pkgs.terminus_font ];
    earlySetup = true;
  };

  services.journald.console = "/dev/tty1";
}
