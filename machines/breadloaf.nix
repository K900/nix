{ lib, pkgs, ... }:
let
  keys = import ../keys.nix;
in
{
  imports = [ ../shared/desktop.nix ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "kvm-intel" ];

    initrd.availableKernelModules = [
      "xhci_pci"
      "ahci"
      "usbhid"
      "usb_storage"
      "sd_mod"
    ];

    loader.bootis = {
      refind.theme.regular = {
        size = "m";
        font.size = 28;
      };
      installAsFallback = true;
    };
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
    enableRedistributableFirmware = true;
    graphics.extraPackages = [ pkgs.intel-media-driver ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos_root";
      fsType = "bcachefs";
      options = [ "compression=zstd,version_upgrade=compatible,discard" ];
    };

    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };
  };

  powerManagement.cpuFreqGovernor = "performance";

  users.users.marina = {
    isNormalUser = true;
    description = "Марина";
    openssh.authorizedKeys.keys = [ keys.k900 ];
    extraGroups = [
      "adbusers"
      "pipewire"
      "wheel"
    ];
    packages = [ pkgs.scrcpy ];
  };

  home-manager.users.marina = {
    imports = [ ../shared/home/graphical.nix ];
    home.stateVersion = "24.05";

    programs = {
      firefox.package = lib.mkForce pkgs.firefox-bin;

      plasma.workspace = lib.mkForce {
        colorScheme = "BreezeLight";
        cursor.theme = "breeze_cursors";
        lookAndFeel = "org.kde.breeze.desktop";
      };
    };

    xdg = {
      mimeApps.defaultApplications = lib.mkForce {
        "text/html" = "firefox.desktop";
        "x-scheme-handler/http" = "firefox.desktop";
        "x-scheme-handler/https" = "firefox.desktop";
        "x-scheme-handler/mailto" = "firefox.desktop";
      };

      userDirs = lib.mkForce {
        desktop = "$HOME/Рабочий стол";
        documents = "$HOME/Документы";
        download = "$HOME/Загрузки";
      };
    };

    gtk = lib.mkForce {
      theme.name = "Breeze";
      cursorTheme.name = "breeze_cursors";
      iconTheme.name = "breeze";
      gtk3.extraConfig.gtk-application-prefer-dark-theme = false;
      gtk4.extraConfig.gtk-application-prefer-dark-theme = false;
    };
  };

  services.displayManager.autoLogin.user = lib.mkForce "marina";

  i18n = {
    defaultLocale = lib.mkForce "ru_RU.UTF-8";
    extraLocaleSettings.LC_TIME = lib.mkForce "ru_RU.UTF-8";
  };
  time.timeZone = lib.mkForce "Europe/Samara";

  system = {
    autoUpgrade = {
      enable = true;
      flake = "gitlab:K900/nix";
      operation = "boot";
      persistent = true;
      flags = [ "--accept-flake-config" ];
    };

    stateVersion = lib.mkForce "24.05";
  };

  services = {
    flatpak.enable = true;
    printing.drivers = [ pkgs.gutenprint ];
  };
}
