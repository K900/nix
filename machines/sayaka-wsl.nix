{ lib, ... }:
{
  imports = [ ../shared/homenet-builders.nix ];

  nix = {
    settings = {
      keep-derivations = true;
      keep-outputs = true;
    };

    buildMachines = [
      {
        protocol = "ssh-ng";
        sshUser = "k900";
        hostName = "darwin-build-box.nix-community.org";
        systems = [
          "aarch64-darwin"
          "x86_64-darwin"
        ];
        maxJobs = 4;
        supportedFeatures = [ "big-parallel" ];
      }
    ];
  };

  systemd.oomd.enable = lib.mkForce true;
}
