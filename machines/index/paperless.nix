{
  age.secrets.paperless_auth_config = {
    file = ../../secrets/paperless_auth_config.age;
    owner = "paperless";
  };

  services = {
    paperless = {
      enable = true;
      settings = {
        PAPERLESS_URL = "https://paperless.ts.0upti.me";
        PAPERLESS_OCR_LANGUAGE = "rus+eng";
        PAPERLESS_APPS = "allauth.socialaccount.providers.openid_connect";
        PAPERLESS_DISABLE_REGULAR_LOGIN = true;
      };
    };

    nginx = {
      upstreams.paperless.servers."127.0.0.1:28981" = { };

      virtualHosts."paperless.ts.0upti.me".locations."/" = {
        proxyPass = "http://paperless";
        proxyWebsockets = true;
      };
    };
  };

  systemd.services.paperless-web.serviceConfig.EnvironmentFile = "/run/agenix/paperless_auth_config";
}
