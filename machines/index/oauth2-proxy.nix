{
  imports = [ ../../shared/applications/server/oauth2-proxy-base.nix ];

  age.secrets.oauth2_proxy_secrets = {
    file = ../../secrets/index_oauth2_proxy_secrets.age;
    owner = "oauth2-proxy";
  };

  services.oauth2-proxy = {
    oidcIssuerUrl = "https://auth.0upti.me/oauth2/openid/index";
    clientID = "index";

    nginx = {
      domain = "index-auth.ts.0upti.me";
      virtualHosts = {
        "bt.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "prowlarr.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "sonarr.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "radarr.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "lidarr.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "bazarr.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "bitmagnet.ts.0upti.me".allowed_groups = [ "pirates@auth.0upti.me" ];
        "syncthing.ts.0upti.me".allowed_groups = [ "sync_users@auth.0upti.me" ];
      };
    };
  };
}
