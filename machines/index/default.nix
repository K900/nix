{ config, pkgs, ... }:
{
  imports = [
    ../../shared/platform/cm3588-nas.nix
    ../../shared/server.nix

    ../../shared/applications/server/acme.nix
    ../../shared/applications/server/nginx.nix
    ../../shared/applications/server/postgres.nix

    ../../shared/applications/monitoring
    ../../shared/applications/monitoring/nginx_exporter.nix
    ../../shared/applications/monitoring/postgres_exporter.nix

    ../../shared/applications/tailscale.nix

    ./oauth2-proxy.nix

    ./media/bazarr.nix
    ./media/bitmagnet.nix
    # FIXME: horked, see https://github.com/NixOS/nixpkgs/issues/332776
    # ./media/flaresolverr.nix
    ./media/group.nix
    ./media/i2p.nix
    ./media/jellyfin.nix
    ./media/ksmbd.nix
    ./media/lidarr.nix
    ./media/prowlarr.nix
    ./media/radarr.nix
    ./media/shadowsocks.nix
    ./media/sonarr.nix
    ./media/transmission.nix
    ./media/yggdrasil.nix

    ./paperless.nix
    ./syncthing.nix

    # ../../utils/upgrade-postgres.nix
  ];

  networking = {
    useDHCP = false;
    firewall.allowedUDPPorts = [ 5353 ]; # mDNS
  };

  systemd.network = {
    enable = true;

    networks.wired = {
      name = "en*";
      DHCP = "yes";
      domains = [ "home" ];
      networkConfig.MulticastDNS = true;
    };
  };

  boot.kernelParams = [
    # Needed to fit in all the NVMe
    "cma=256M"
    # Redirect console to a UART that's not weirdly shorted
    "console=feb90000.serial:0.0,1500000n8"
  ];

  # FIXME: ugly workaround for systemd not understanding multi-device filesystems
  systemd.services.mount-media = rec {
    description = "Mount /media";

    bindsTo = [
      "dev-nvme0n1.device"
      "dev-nvme1n1.device"
      "dev-nvme2n1.device"
      "dev-nvme3n1.device"
    ];
    after = bindsTo ++ [ "local-fs-pre.target" ];

    wantedBy = [ "local-fs.target" ];
    before = [
      "umount.target"
      "local-fs.target"
    ];

    conflicts = [ "umount.target" ];

    unitConfig.DefaultDependencies = false;

    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStart = "${pkgs.bcachefs-tools}/bin/bcachefs mount /dev/nvme0n1p1:/dev/nvme1n1p1:/dev/nvme2n1p1:/dev/nvme3n1p1 /media -o compression=zstd,version_upgrade=compatible,discard,errors=fix_safe";
      ExecStop = "${pkgs.util-linux}/bin/umount /media";
    };

    restartIfChanged = false;
  };
  environment.systemPackages = [ pkgs.bcachefs-tools ];

  services.postgresql.dataDir = "/media/postgresql/${config.services.postgresql.package.psqlSchema}";

  hardware.enableRedistributableFirmware = true;
}
