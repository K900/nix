{
  services = {
    syncthing = {
      enable = true;
      dataDir = "/media/Syncthing";
      openDefaultPorts = true;

      settings.gui.insecureSkipHostcheck = true;
    };

    nginx = {
      upstreams.syncthing.servers."127.0.0.1:8384" = { };

      virtualHosts."syncthing.ts.0upti.me".locations."/" = {
        proxyPass = "http://syncthing";
        proxyWebsockets = true;
      };
    };
  };
}
