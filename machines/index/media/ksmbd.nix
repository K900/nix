{ pkgs, ... }:
{
  environment.systemPackages = [ pkgs.ksmbd-tools ];

  systemd = {
    packages = [ pkgs.ksmbd-tools ];
    services.ksmbd.wantedBy = [ "multi-user.target" ];
  };

  # enable wsdd to be discoverable by Windows clients
  services.samba-wsdd = {
    enable = true;
    openFirewall = true;
    interface = "enP4p65s0";
  };

  environment.etc = {
    "ksmbd/ksmbd.conf".text = ''
      [global]
      server multi channel support = yes

      [media]
      comment = media share
      path = /media
      writeable = yes
      inherit owner = yes
      guest ok = yes
      force user = media
      force group = media
    '';

    # user guest, no password
    "ksmbd/ksmbdpwd.db".text = ''
      guest:MdbP4NFq6TG3PFnX4MCJwA==
    '';

    # enable mDNS to be discoverable by normal clients
    "systemd/dnssd/smb.dnssd".text = ''
      [Service]
      Name=%H
      Type=_smb._tcp
      Port=445
    '';
  };

  networking.firewall.interfaces.enP4p65s0.allowedTCPPorts = [ 445 ];
}
