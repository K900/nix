{
  services.yggdrasil = {
    enable = true;
    persistentKeys = true;
    settings.Peers = [
      "tcp://77.37.218.131:12402"
      "tcp://178.20.41.3:65533"
    ];
  };
}
