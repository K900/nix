{ inputs, pkgs, ... }:
{
  age.secrets.transmission = {
    file = ../../../secrets/transmission.age;
    owner = "media";
  };

  services = {
    transmission = {
      enable = true;
      package = pkgs.transmission_4;

      user = "media";
      credentialsFile = "/run/agenix/transmission";

      openPeerPorts = true;
      openRPCPort = false;

      settings = {
        download-dir = "/media/Torrents";
        incomplete-dir-enabled = false;

        rpc-bind-address = "0.0.0.0";
        rpc-port = 8081;

        rpc-authentication-required = true;
        rpc-username = "transmission";

        rpc-host-whitelist-enabled = false;
        rpc-whitelist-enabled = false;

        default-trackers = builtins.readFile "${inputs.trackerlist}/trackers_all.txt";
      };
    };
  };

  services.nginx = {
    upstreams.transmission.servers."127.0.0.1:8081" = { };

    virtualHosts."bt.ts.0upti.me".locations."/" = {
      proxyPass = "http://transmission";
      proxyWebsockets = true;
    };
  };

  networking.firewall.interfaces.tailscale0.allowedTCPPorts = [ 8081 ];
}
