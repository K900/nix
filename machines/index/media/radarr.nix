{
  services = {
    radarr = {
      enable = true;
      user = "media";
    };

    nginx = {
      upstreams.radarr.servers."127.0.0.1:7878" = { };

      virtualHosts."radarr.ts.0upti.me".locations."/" = {
        proxyPass = "http://radarr";
        proxyWebsockets = true;
      };
    };
  };
}
