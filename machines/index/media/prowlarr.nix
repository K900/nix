{
  services = {
    prowlarr.enable = true;

    nginx = {
      upstreams.prowlarr.servers."127.0.0.1:9696" = { };

      virtualHosts."prowlarr.ts.0upti.me".locations."/" = {
        proxyPass = "http://prowlarr";
        proxyWebsockets = true;
      };
    };
  };
}
