{
  services.i2pd = {
    enable = true;
    proto = {
      http.enable = true;
      sam.enable = true;
      socksProxy.enable = true;
    };
  };
}
