{
  services = {
    bitmagnet = {
      enable = true;
      openFirewall = true;

      settings = {
        # don't load the network too hard (default: 10)
        dht_crawler.scaling_factor = 3;
        # don't index stuff we don't care about
        classifier.flags.delete_content_types = [
          "game"
          "software"
          "xxx"
        ];
      };
    };

    nginx = {
      upstreams.bitmagnet.servers."127.0.0.1:3333" = { };

      virtualHosts."bitmagnet.ts.0upti.me".locations."/" = {
        proxyPass = "http://bitmagnet";
        proxyWebsockets = true;
      };
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "bitmagnet";
        scrape_configs = [
          {
            job_name = "bitmagnet";
            static_configs = [ { targets = [ "127.0.0.1:3333" ]; } ];
          }
        ];
      }
    ];
  };
}
