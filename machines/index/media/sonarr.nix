{
  services = {
    sonarr = {
      enable = true;
      user = "media";
    };

    nginx = {
      upstreams.sonarr.servers."127.0.0.1:8989" = { };

      virtualHosts."sonarr.ts.0upti.me".locations."/" = {
        proxyPass = "http://sonarr";
        proxyWebsockets = true;
      };
    };
  };
}
