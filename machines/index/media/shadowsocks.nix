{ pkgs, ... }:
{
  age.secrets.shadowsocks-client.file = ../../../secrets/shadowsocks-client.age;

  systemd.services.shadowsocks-client = {
    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    script = "${pkgs.shadowsocks-rust}/bin/sslocal -c /run/agenix/shadowsocks-client";
    wantedBy = [ "multi-user.target" ];
  };
}
