{
  services = {
    lidarr = {
      enable = true;
      user = "media";
    };

    nginx = {
      upstreams.lidarr.servers."127.0.0.1:8686" = { };

      virtualHosts."lidarr.ts.0upti.me".locations."/" = {
        proxyPass = "http://lidarr";
        proxyWebsockets = true;
      };
    };
  };
}
