{
  services = {
    bazarr = {
      enable = true;
      user = "media";
    };

    nginx = {
      upstreams.bazarr.servers."127.0.0.1:6767" = { };

      virtualHosts."bazarr.ts.0upti.me".locations."/" = {
        proxyPass = "http://bazarr";
        proxyWebsockets = true;
      };
    };
  };
}
