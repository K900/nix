{ lib, pkgs, ... }:
let
  msiScraper = pkgs.writers.writePython3Bin "msi-scrape" {
    libraries = [ pkgs.python3Packages.httpx ];
  } (builtins.readFile ./msi.py);
in
{
  imports = [ ./default.nix ];

  age.secrets.urlwatch.file = ../../../secrets/urlwatch.age;

  services.urlwatch = {
    enable = true;
    schedule = "*:0/15";

    settingsFile = "\${CREDENTIALS_DIRECTORY}/urlwatch.yaml";

    jobs = [
      {
        name = "kernel.org";
        url = "https://kernel.org/";
        filter = [
          { css = "#releases td:nth-child(2)"; }
          "html2text"
          "strip"
          { grepi = "next"; }
        ];
      }
      {
        name = "linux-firmware";
        url = "https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/refs/";
        filter = [
          { css = "table.list td:nth-child(1)"; }
          "html2text"
          "strip"
        ];
      }
      {
        name = "steamdeck";
        url = "https://steamdeck-images.steamos.cloud/steamdeck/";
        filter = [
          { css = "td.link"; }
          "html2text"
          "strip"
        ];
      }
      {
        name = "steamdeck-packages";
        url = "https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/";
        filter = [
          { css = "td.link"; }
          "html2text"
          "strip"
        ];
      }
      {
        name = "steamdeck-packages-holo";
        url = "https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/holo-main/";
        filter = [
          { css = "td.link"; }
          "html2text"
          "strip"
        ];
      }
      {
        name = "msi-drivers";
        command = "${lib.getExe msiScraper} 'product=MPG-X670E-CARBON-WIFI&type=driver&os=Win11 64'";
        filter = [ "format-json" ];
        max_tries = 5;
      }
      {
        name = "msi-bios";
        command = "${lib.getExe msiScraper} 'product=MPG-X670E-CARBON-WIFI&type=bios'";
        filter = [ "format-json" ];
        max_tries = 5;
      }
      {
        name = "8bitdo-firmware";
        url = "https://tempfiles.8bitdo.com/PKPK/";
        filter = [
          { css = "a"; }
          "html2text"
          "strip"
          { grep = "^(Pro2V|UltimatePC)"; }
        ];
      }
      {
        name = "8bitdo-receiver-firmware";
        url = "https://tempfiles.8bitdo.com/SPSP/";
        filter = [
          { css = "a"; }
          "html2text"
          "strip"
        ];
      }
      {
        name = "asus-firmware";
        url = "https://rog.asus.com/support/webapi/product/GetPDBIOS?website=global&model=rog-zephyrus-g14-2022&pdid=0&m1id=20279&cpu=GA402RJ&LevelTagId=140049&systemCode=rog";
        filter = [ "format-json" ];
      }
    ];
  };

  systemd.services.urlwatch.serviceConfig.LoadCredential = "urlwatch.yaml:/run/agenix/urlwatch";
}
