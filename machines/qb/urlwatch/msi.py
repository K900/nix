import httpx
import sys

headers = {
    'User-Agent': 'Mozilla/5.0 '
                  '(Windows NT 10.0; Win64; x64; rv:130.0) '
                  'Gecko/20100101 Firefox/130.0',
    'Accept': 'application/json, text/plain, */*'
}

with httpx.Client(headers=headers, base_url='https://www.msi.com/api') as h:
    h.get('/check/ip/language')
    print(h.get('/v1/product/support/panel?' + sys.argv[1]).text)
