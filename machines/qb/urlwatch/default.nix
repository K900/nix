{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.services.urlwatch;
  yaml = pkgs.formats.yaml { };

  settingsFile = yaml.generate "urlwatch-settings.yaml" cfg.settings;
  jobsConfig = builtins.concatStringsSep "\n---\n" (builtins.map builtins.toJSON cfg.jobs);
  jobsFile = pkgs.writeText "urlwatch-jobs.yaml" jobsConfig;
in
{
  options = {
    services.urlwatch = {
      enable = lib.mkEnableOption (lib.mdDoc "urlwatch URL monitoring service");

      settings = lib.mkOption {
        description = lib.mdDoc "urlwatch general settings, see https://urlwatch.readthedocs.io/en/latest/configuration.html";
        inherit (yaml) type;
      };

      settingsFile = lib.mkOption {
        description = lib.mdDoc "urlwatch settings file path, overrides `settings`";
        type = lib.types.nullOr lib.types.str;
        default = null;
      };

      jobs = lib.mkOption {
        description = lib.mdDoc "urlwatch job definitions, see https://urlwatch.readthedocs.io/en/latest/jobs.html";
        type = lib.types.listOf yaml.type;
      };

      cache = lib.mkOption {
        description = lib.mdDoc "urlwatch cache location or Redis URL";
        type = lib.types.str;
        default = "/var/lib/urlwatch/urlwatch.db";
      };

      schedule = lib.mkOption {
        description = lib.mdDoc "schedule for urlwatch timer, in systemd OnCalendar= format";
        type = lib.types.str;
        default = "*:0";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    systemd = {
      services.urlwatch = {
        description = "Watch URLs for changes";
        wantedBy = [ "multi-user.target" ];
        after = [ "network-online.target" ];
        wants = [ "network-online.target" ];
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.urlwatch}/bin/urlwatch --urls=%d/jobs.yaml --config=${
            if cfg.settingsFile != null then cfg.settingsFile else settingsFile
          } --cache=${cfg.cache}";
          DynamicUser = true;
          StateDirectory = "urlwatch";
          LoadCredential = [ "jobs.yaml:${jobsFile}" ];
        };
      };

      timers.urlwatch = {
        description = "Watch URLs for changes";
        wantedBy = [ "timers.target" ];
        timerConfig.OnCalendar = cfg.schedule;
      };
    };
  };
}
