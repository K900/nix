{
  services.nginx.virtualHosts."succ.ts.0upti.me".locations."/" = {
    proxyPass = "http://192.168.4.3"; # robot vacuum
    proxyWebsockets = true;
  };
}
