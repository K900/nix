{ lib, pkgs, ... }:
{
  services = {
    home-assistant = {
      enable = true;

      extraComponents = [
        "androidtv"
        "androidtv_remote"
        "cast" # required by ... something?
        "lg_thinq"
        "met"
        "mobile_app"
        "mqtt"
        "prometheus"
        "ssdp" # required by webostv
        "tasmota"
        "webostv"
      ];

      customComponents = with pkgs.home-assistant-custom-components; [
        midea_ac_lan
        moonraker
        tuya_local
        yassi
      ];

      customLovelaceModules = with pkgs.home-assistant-custom-lovelace-modules; [
        button-card
        decluttering-card
        lg-webos-remote-control
        universal-remote-card
        valetudo-map-card
        (pkgs.callPackage ./cards { })
      ];

      lovelaceConfig = lib.importJSON (
        pkgs.runCommand "lovelace.json" { nativeBuildInputs = [ pkgs.yq ]; } ''
          yq . < ${./lovelace.yaml} > $out
        ''
      );

      config = {
        http = {
          use_x_forwarded_for = true;
          server_host = "127.0.0.1";
          trusted_proxies = "127.0.0.1";
        };

        homeassistant = {
          unit_system = "metric";
          time_zone = "Europe/Moscow";
          temperature_unit = "C";
          longitude = "37.6173";
          latitude = "55.7558";

          auth_providers = [
            {
              type = "command_line";
              command = lib.getExe (
                pkgs.writers.writePython3Bin "hass-auth" { libraries = [ pkgs.python3Packages.ldap3 ]; } (
                  builtins.readFile ./ldap_auth.py
                )
              );
              meta = true;
            }
          ];
        };

        # selected bits of default config
        backup = { };
        cloud = { }; # required by smartthings integration for auth
        config = { };
        history = { };
        homeassistant_alerts = { };
        mobile_app = { };
        my = { };
        prometheus = {
          requires_auth = false;
        };
        sun = { };

        mqtt = lib.importJSON ./kettle.json;

        "automation ui" = "!include automations.yaml";
      };
    };

    nginx = {
      upstreams.hass.servers."127.0.0.1:8123" = { };

      virtualHosts."hass.ts.0upti.me".locations."/" = {
        proxyPass = "http://hass";
        proxyWebsockets = true;
      };
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "hass";
        scrape_configs = [
          {
            job_name = "hass";
            metrics_path = "/api/prometheus";
            static_configs = [
              {
                targets = [ "localhost:8123" ];
              }
            ];
          }
        ];
      }
    ];
  };
}
