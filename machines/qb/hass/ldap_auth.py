import os
import sys

from ldap3 import Connection, Server


SERVER = 'ldaps://auth.0upti.me'
BASE_DN = 'dc=auth,dc=0upti,dc=me'
FILTER = '(uid={user})'
ADMINS_GROUP = 'spn=homeowners@auth.0upti.me,dc=auth,dc=0upti,dc=me'
USERS_GROUP = 'spn=home_users@auth.0upti.me,dc=auth,dc=0upti,dc=me'


def main():
    user = os.environ['username']
    password = os.environ['password']

    with Connection(
        Server(SERVER),
        user=f"uid={user}",
        password=password,
    ) as ldap:
        ldap.search(
            BASE_DN,
            FILTER.format(user=user),
            attributes=['displayname', 'memberof']
        )

        if not ldap.entries:
            sys.exit(1)

        entry = ldap.entries[0]

        print(f'name = {entry.displayname}')

        if ADMINS_GROUP in entry.memberof:
            print('group = system-admin')
        elif USERS_GROUP in entry.memberof:
            print('group = system-users')
        else:
            sys.exit(1)


if __name__ == '__main__':
    main()
