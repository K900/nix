{ pkgs, ... }:
{
  systemd.services.flashmq = {
    wantedBy = [ "multi-user.target" ];
    script = ''
      ${pkgs.flashmq}/bin/flashmq --config ${./flashmq.conf};
    '';
  };

  networking.firewall.allowedTCPPorts = [ 1883 ];
}
