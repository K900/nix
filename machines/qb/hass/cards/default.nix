{ runCommand }:
runCommand "my-hass-cards"
  {
    version = "0.1";
    passthru.entrypoint = "all-cards.js";
  }
  ''
    mkdir $out
    cp ${./.}/*.js $out/
  ''
