{
  imports = [ ../../shared/applications/server/oauth2-proxy-base.nix ];

  age.secrets.oauth2_proxy_secrets = {
    file = ../../secrets/qb_oauth2_proxy_secrets.age;
    owner = "oauth2-proxy";
  };

  services.oauth2-proxy = {
    oidcIssuerUrl = "https://auth.0upti.me/oauth2/openid/qb";
    clientID = "qb";

    nginx = {
      domain = "qb-auth.ts.0upti.me";
      virtualHosts."succ.ts.0upti.me".allowed_groups = [ "homeowners@auth.0upti.me" ];
    };
  };
}
