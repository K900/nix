{
  services = {
    mimir = {
      enable = true;

      configuration = {
        multitenancy_enabled = false;

        common.storage.backend = "filesystem";

        server = {
          http_listen_port = 9000;
          grpc_listen_port = 9001;
          grpc_server_max_recv_msg_size = 104857600;
          grpc_server_max_send_msg_size = 104857600;
          grpc_server_max_concurrent_streams = 1000;
        };

        ingester.ring.replication_factor = 1;

        distributor.instance_limits.max_ingestion_rate = 0; # unlimited

        limits = {
          ingestion_rate = 1000000; # can't set to unlimited :(
          out_of_order_time_window = "12h";
          max_global_series_per_user = 0; # unlimited
          max_label_value_length = 10000; # we have pgscv queries that are LONG
        };
      };
    };

    nginx = {
      upstreams.mimir.servers."127.0.0.1:9000" = { };

      virtualHosts."mimir.ts.0upti.me".locations."/" = {
        proxyPass = "http://mimir";
        proxyWebsockets = true;
      };
    };
  };

  systemd.services = {
    mimir = {
      wants = [ "network-online.target" ];
      after = [ "network-online.target" ];
    };

    grafana-agent.after = [ "mimir.service" ];
  };
}
