let
  makeJobConfig =
    {
      name,
      module,
      targets,
    }:
    {
      job_name = name;
      metrics_path = "/probe";
      params.module = [ module ];
      static_configs = [ { inherit targets; } ];
      relabel_configs = [
        {
          source_labels = [ "__address__" ];
          target_label = "__param_target";
        }
        {
          source_labels = [ "__param_target" ];
          target_label = "instance";
        }
        {
          target_label = "__address__";
          replacement = "localhost:9005";
        }
      ];
    };
in
{
  services = {
    prometheus.exporters.blackbox = {
      enable = true;
      configFile = ./blackbox_exporter.yaml;
      port = 9005;
      extraFlags = [ "--log.level=debug" ];
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "blackbox";
        scrape_configs = map makeJobConfig [
          {
            name = "blackbox";
            module = "http_2xx";
            targets = [
              "https://google.com/"
              "https://api.telegram.org/"
            ];
          }

          {
            name = "ipv4";
            module = "icmp_v4";
            targets = [ "google.com" ];
          }

          # {
          #   name = "ipv6";
          #   module = "icmp_v6";
          #   targets = ["google.com"];
          # }
        ];
      }
    ];
  };
}
