{
  age.secrets = {
    grafana_client_secret = {
      file = ../../../secrets/grafana_client_secret.age;
      owner = "grafana";
    };
    grafana_contact_points = {
      file = ../../../secrets/grafana_contact_points.age;
      owner = "grafana";
    };
  };

  services = {
    grafana = {
      enable = true;

      settings = {
        server = {
          domain = "grafana.ts.0upti.me";
          http_addr = "127.0.0.1";
          http_port = 2342;
          root_url = "https://grafana.ts.0upti.me/";
        };

        database = {
          type = "postgres";
          user = "grafana";
          host = "/run/postgresql";
        };

        "auth.generic_oauth" = {
          enabled = true;

          name = "Kanidm";
          client_id = "grafana";
          client_secret = "$__file{/run/agenix/grafana_client_secret}";

          auth_url = "https://auth.0upti.me/ui/oauth2";
          token_url = "https://auth.0upti.me/oauth2/token";
          api_url = "https://auth.0upti.me/oauth2/openid/grafana/userinfo";

          login_attribute_path = "preferred_username";
          scopes = [
            "openid"
            "profile"
            "email"
            "groups"
          ];

          use_pkce = true;
          use_refresh_token = true;
          allow_sign_up = true;
          auto_login = true;
          allow_assign_grafana_admin = true;

          role_attribute_path = "contains(groups[*], 'grafana_admins@auth.0upti.me') && 'GrafanaAdmin' || 'Viewer'";
        };

        dashboards.default_home_dashboard_path = "${./dashboards/home.json}";

        feature_toggles.enable = "autoMigrateOldPanels newVizTooltips";
        security.angular_support_enabled = false;
      };

      provision = {
        dashboards.settings = {
          apiVersion = 1;
          providers = [
            {
              name = "default";
              options.path = ./dashboards;
            }
          ];
        };

        datasources.settings = {
          apiVersion = 1;
          datasources = [
            {
              name = "Mimir";
              type = "prometheus";
              uid = "mimir";
              access = "proxy";
              url = "http://127.0.0.1:9000/prometheus";
              isDefault = true;
            }
            {
              name = "Loki";
              type = "loki";
              uid = "loki";
              access = "proxy";
              url = "http://127.0.0.1:9090/";
            }
            {
              name = "Tempo";
              type = "tempo";
              uid = "tempo";
              access = "proxy";
              url = "http://127.0.0.1:9190/";
            }
          ];
        };

        alerting = {
          contactPoints.path = "/run/agenix/grafana_contact_points";
          rules.settings = {
            apiVersion = 1;

            groups =
              let
                readRule = path: builtins.fromJSON (builtins.readFile path);
                readRules = builtins.map readRule;
              in
              [
                {
                  name = "Home";
                  folder = "Alerts";
                  interval = "30s";
                  rules = readRules [ ./rules/co2.json ];
                }
                {
                  name = "Node";
                  folder = "Alerts";
                  interval = "30s";
                  rules = readRules [
                    ./rules/node_down.json
                    ./rules/unit_down.json
                    ./rules/network_down.json
                    ./rules/scrape_failed.json
                    ./rules/no_logs.json
                    ./rules/matrix_federation_down.json
                  ];
                }
              ];
          };
          policies.settings = {
            apiVersion = 1;
            policies = [
              {
                receiver = "Telegram";
                group_by = [
                  "grafana_folder"
                  "alertname"
                ];
              }
            ];
          };
        };
      };
    };

    postgresql = {
      ensureDatabases = [ "grafana" ];
      ensureUsers = [
        {
          name = "grafana";
          ensureDBOwnership = true;
        }
      ];
    };

    nginx = {
      upstreams.grafana.servers."127.0.0.1:2342" = { };

      virtualHosts."grafana.ts.0upti.me".locations."/" = {
        proxyPass = "http://grafana";
        proxyWebsockets = true;
      };
    };
  };
}
