{
  services.grafana-agent.settings.metrics.configs = [
    {
      name = "sayaka";
      scrape_configs = [
        {
          job_name = "sayaka";
          static_configs = [
            {
              targets = [
                "sayaka-w:9182" # ohmgraphite
                "sayaka-w:9183" # wmi_exporter
              ];
            }
          ];
        }
      ];
    }
  ];
}
