{
  services.grafana-agent.settings.metrics.configs = [
    {
      name = "akane";
      scrape_configs = [
        {
          job_name = "akane";
          static_configs = [
            {
              targets = [
                "akane-w:9182" # ohmgraphite
                "akane-w:9183" # wmi_exporter
              ];
            }
          ];
        }
      ];
    }
  ];
}
