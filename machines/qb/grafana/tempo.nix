{
  services.tempo = {
    enable = true;
    settings = {
      server = {
        http_listen_address = "0.0.0.0";
        http_listen_port = 9190;
        grpc_listen_address = "0.0.0.0";
        grpc_listen_port = 9195;
      };
      distributor.receivers.otlp.protocols.http.endpoint = "0.0.0.0:4318";
      storage.trace = {
        backend = "local";
        wal.path = "/var/lib/tempo/wal";
        local.path = "/var/lib/tempo/blocks";
      };
    };
  };

  networking.firewall.interfaces.tailscale0.allowedTCPPorts = [ 4318 ];
}
