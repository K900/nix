{ inputs, ... }:
{
  imports = [
    ../../shared/platform/orangepi5max.nix
    ../../shared/server.nix

    ../../shared/applications/server/acme.nix
    ../../shared/applications/server/nginx.nix
    ../../shared/applications/server/postgres.nix

    ../../shared/applications/monitoring
    ../../shared/applications/monitoring/nginx_exporter.nix
    ../../shared/applications/monitoring/postgres_exporter.nix

    ../../shared/applications/tailscale.nix

    ./oauth2-proxy.nix

    ./grafana/grafana.nix
    ./grafana/loki.nix
    ./grafana/mimir.nix
    ./grafana/tempo.nix

    ./grafana/blackbox_exporter.nix

    ./grafana/scrape_akane.nix
    ./grafana/scrape_sayaka.nix

    inputs.scd4x-exporter.nixosModules.default

    ./hass/hass.nix
    ./hass/flashmq
    ./succ-proxy.nix

    ./urlwatch/config.nix

    # ../../utils/upgrade-postgres.nix
  ];

  networking = {
    useDHCP = false;
    firewall.allowedUDPPorts = [ 5353 ]; # mDNS
  };

  systemd = {
    network = {
      enable = true;
      wait-online.anyInterface = true;

      networks.wired = {
        name = "en*";
        DHCP = "yes";
        domains = [ "home" ];
        networkConfig.MulticastDNS = true;
      };
    };

    services.nix-daemon.environment.TMPDIR = "/var/tmp/nix";
  };

  hardware = {
    enableRedistributableFirmware = true;

    deviceTree.overlays = [
      {
        name = "opi5max-i2c";
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "xunlong,orangepi-5-max";
          };

          &i2c2 {
            pinctrl-0 = <&i2c2m0_xfer>;
            status = "okay";
          };
        '';
      }

      {
        name = "opi5max-fan";
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "xunlong,orangepi-5-max";
          };

          &pwm0 {
            pinctrl-names = "default";
            pinctrl-0 = <&pwm0m2_pins>;
            status = "okay";
          };

          &fan {
            cooling-levels = <0 0 120 200 255>;
            pwms = <&pwm0 0 40000 0>;
            interrupt-parent = <&gpio1>;
            interrupts = <10 2>; // RK_PB2 IRQ_TYPE_EDGE_FALLING
            pulses-per-revolution = <2>;
          };
        '';
      }
    ];
  };

  services = {
    scd4x-exporter = {
      enable = true;
      i2cDevice = "/dev/i2c-2";
      listenAddress = "0.0.0.0:9099";
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "scd4x";
        scrape_configs = [
          {
            job_name = "scd4x";
            static_configs = [ { targets = [ "localhost:9099" ]; } ];
          }
        ];
      }
    ];
  };
}
