COLMENA_FLAGS := "--nix-option max-jobs 4 --nix-option cores 16 --show-trace --verbose --keep-result --experimental-flake-eval"

default: update deploy

deploy TARGETS="konata,morgana,qb,index,bananya" +ARGS="":
    @colmena apply --on {{TARGETS}} {{COLMENA_FLAGS}} {{ARGS}}

update:
    @nix flake update --commit-lock-file

diff TARGET:
    @ssh {{TARGET}} nix profile diff-closures --profile /nix/var/nix/profiles/system

wat OPTION HOST="$(hostname)":
    @nix eval ".#nixosConfigurations.{{HOST}}.config.{{OPTION}}"

build-all +ARGS="":
    @colmena build {{COLMENA_FLAGS}} {{ARGS}}

push +ARGS="": (build-all ARGS)
    @cachix push 0uptime .gcroots/*

sd TARGET:
    @nom build ".#nixosConfigurations.{{TARGET}}.config.system.build.sdImage" --verbose -j4 --show-trace -L

provision:
    sudo ln -s {{justfile_directory()}}/flake.nix /etc/nixos/flake.nix
