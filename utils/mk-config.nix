{
  inputs,
  patches ? _: { },
  hostSystem ? builtins.currentSystem,
}:
let
  pkgsForPatching = import inputs.nixpkgs { system = hostSystem; };

  patchFetchers = rec {
    pr =
      repo: id: hash:
      pkgsForPatching.fetchpatch2 {
        url = "https://github.com/${repo}/pull/${builtins.toString id}.diff";
        inherit hash;
      };
    npr = pr "NixOS/nixpkgs";
    jpr = pr "Jovian-Experiments/Jovian-NixOS";
  };

  fetchedPatches = patches patchFetchers;

  patchInput =
    name: value:
    if (fetchedPatches.${name} or [ ]) != [ ] then
      let
        patchedSrc = pkgsForPatching.applyPatches {
          name = "source";
          src = value;
          patches = fetchedPatches.${name};
        };
      in
      # wasFlake = (value._type or "plain") == "flake";
      # if wasFlake
      # then builtins.getFlake patchedSrc
      # else patchedSrc
      patchedSrc
    else
      value;

  patchedInputs = builtins.mapAttrs patchInput inputs;

  patchedNixpkgs = import patchedInputs.nixpkgs;
  patchedNixpkgsHost = patchedNixpkgs { system = hostSystem; };

  specialArgs = {
    inputs = patchedInputs;
    rawInputs = inputs;
    pkgsHost = patchedNixpkgsHost;
  };

  mkSystem =
    {
      hostname,
      extraModules ? [ ],
      system ? "x86_64-linux",
      allowLocalDeployment ? false,
    }:
    let
      machineSpecificConfig = ../machines + "/${hostname}.nix";
      machineSpecificConfigDir = ../machines + "/${hostname}/default.nix";
      machineSpecificModules =
        if builtins.pathExists machineSpecificConfig then
          [ machineSpecificConfig ]
        else if builtins.pathExists machineSpecificConfigDir then
          [ machineSpecificConfigDir ]
        else
          [ ];
    in
    {
      imports =
        [
          {
            nixpkgs = {
              config = {
                allowAliases = false;
                allowUnfree = true;
              };
              hostPlatform = system;
            };

            deployment = {
              inherit allowLocalDeployment;
              targetHost = hostname;
            };

            networking.hostName = hostname;
          }
        ]
        ++ machineSpecificModules
        ++ extraModules;
    };
  mkWslSystem =
    { hostname }:
    mkSystem {
      hostname = "${hostname}-wsl";
      extraModules = [ ../shared/wsl.nix ];
      allowLocalDeployment = true;
    };

  hiveMeta = {
    nixpkgs = patchedNixpkgsHost;
    inherit specialArgs;
  };

  mkHome =
    {
      username,
      homeDirectory ? "/home/${username}",
      extraModules ? [ ],
      system ? "x86_64-linux",
    }:
    patchedInputs.home-manager.lib.homeManagerConfiguration {
      pkgs = patchedNixpkgs { inherit system; };
      modules = [
        ../shared/home/standalone.nix
        {
          home = {
            inherit username homeDirectory;
          };
        }
      ] ++ extraModules;
      extraSpecialArgs = specialArgs;
    };
in
{
  inherit
    hiveMeta
    mkSystem
    mkWslSystem
    mkHome
    ;
  pkgs = patchedNixpkgsHost;
}
