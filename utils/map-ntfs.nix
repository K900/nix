pkgs:
pkgs.lib.mapAttrs' (
  name: value:
  pkgs.lib.nameValuePair "/mnt/${name}" {
    device = "/dev/disk/by-label/${value}";
    fsType = "ntfs3";
    options = [ "nofail,uid=1000,gid=1000" ];
  }
)
