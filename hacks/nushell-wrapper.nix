{
  nixpkgs.overlays = [
    (final: _: {
      nushell-wrapped =
        (final.writeTextFile {
          name = "nushell-wrapped";

          executable = true;
          destination = "/bin/nu";

          text = ''
            #!/usr/bin/env -S bash --login
            exec ${final.nushell}/bin/nu "$@"
          '';
        })
        // {
          shellPath = "/bin/nu";
        };
    })
  ];
}
