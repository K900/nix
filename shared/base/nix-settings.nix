{ inputs, ... }:
{
  nix = {
    # pin all the nixpkgs to the version in the flake
    registry = {
      self.flake = inputs.self;
      nixpkgs.flake = inputs.nixpkgs;
    };

    settings = {
      show-trace = true;
      keep-going = true;

      experimental-features = [
        "nix-command"
        "flakes"
      ];

      builders-use-substitutes = true;
      log-lines = 9999;

      substituters = [
        "https://cache.nixos.org"
        "https://0uptime.cachix.org"
      ];
      trusted-public-keys = [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "0uptime.cachix.org-1:ctw8yknBLg9cZBdqss+5krAem0sHYdISkw/IFdRbYdE="
      ];

      connect-timeout = 5;
    };
  };
}
