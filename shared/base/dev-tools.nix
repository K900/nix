{
  config,
  inputs,
  options,
  pkgs,
  ...
}:
{
  services = {
    nixseparatedebuginfod = {
      enable = true;
      nixPackage = config.nix.package;
    };
    pcscd.enable = true;
  };

  systemd = {
    services = {
      # shared ssh-agent
      ssh-agent = {
        wantedBy = [ "multi-user.target" ];
        environment.SSH_AUTH_SOCK = config.environment.variables.SSH_AUTH_SOCK;
        serviceConfig = {
          ExecStartPre = "${pkgs.coreutils}/bin/rm -f $SSH_AUTH_SOCK";
          ExecStart = "${pkgs.openssh}/bin/ssh-agent -D -a $SSH_AUTH_SOCK";
          User = "k900";
        };
      };

      nix-daemon.environment = {
        inherit (config.environment.variables) SSH_AUTH_SOCK;
        TMPDIR = "/var/tmp/nix";
      };
    };

    # Clean up /var/tmp/nix way more often
    tmpfiles.rules = [ "d /var/tmp/nix 1777 root root 1d" ];
  };

  virtualisation.podman = {
    enable = true;
    defaultNetwork.settings.dns_enabled = true;
    dockerSocket.enable = true;
  };

  programs = {
    adb.enable = true;
    command-not-found.enable = false; # replaced with nix-index
    nix-ld = {
      enable = true;

      # for Slint language server
      libraries = options.programs.nix-ld.libraries.default ++ [
        pkgs.fontconfig
        pkgs.freetype
        pkgs.libinput
        pkgs.libxkbcommon
        pkgs.mesa
        pkgs.udev
      ];
    };
  };

  environment.variables = {
    # always use rootful podman
    CONTAINER_HOST = "unix:///run/podman/podman.sock";
    # use shared ssh agent
    SSH_AUTH_SOCK = "/tmp/ssh-agent.socket";
  };

  nix.package = inputs.lix.packages.${pkgs.stdenv.hostPlatform.system}.default;

  # allow podman without sudo for minikube
  security.sudo-rs.extraConfig = ''
    %podman ALL=(ALL) NOPASSWD: /run/current-system/sw/bin/podman
  '';

  home-manager.users.k900.imports = [ ../home/dev-tools.nix ];

  users.users.k900.extraGroups = [
    "adbusers"
    "podman"
    "wireshark"
  ];
}
