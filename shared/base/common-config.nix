{
  inputs,
  lib,
  pkgs,
  rawInputs,
  ...
}:
let
  keys = import ../../keys.nix;
in
{
  imports = [
    inputs.agenix.nixosModules.age
    inputs.home-manager.nixosModules.home-manager
    ./diff-on-activation.nix
    ./nix-settings.nix
    ../../hacks/nushell-wrapper.nix
    ../../shared/magic-gc/nixos.nix
  ];

  boot = {
    tmp = {
      useTmpfs = true;
      # make sure we can fit builds
      # this is fine because at least some of it will live in zram
      tmpfsSize = "100%";
    };

    initrd.systemd = {
      enable = true;
      emergencyAccess = true;
    };

    # enable TCP BBR for hopefully better utilization
    kernel.sysctl."net.ipv4.tcp_congestion_control" = "bbr";
  };

  i18n.defaultLocale = "en_US.UTF-8";

  zramSwap = {
    enable = true;
    swapDevices = 1;
    algorithm = "zstd";
  };

  services = {
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        AllowAgentForwarding = true;
      };
    };

    dbus.implementation = "broker";
    journald.extraConfig = "SystemMaxUse=100M";

    orca.enable = false;
    speechd.enable = false;
  };

  nix = {
    package = lib.mkDefault pkgs.lix;

    nixPath = [ "nixpkgs=flake:nixpkgs" ];

    distributedBuilds = true;
    daemonCPUSchedPolicy = "batch";

    settings.trusted-users = [ "k900" ];
  };

  programs.ssh.knownHosts = lib.mapAttrs (_: v: { publicKey = v; }) (import ../../keys.nix);

  environment.shells = [ pkgs.nushell-wrapped ];

  users = {
    mutableUsers = false;

    users = {
      root = {
        shell = pkgs.nushell-wrapped;
        openssh.authorizedKeys.keys = [ keys.k900 ];
      };
      k900 = {
        isNormalUser = true;
        hashedPassword = "$y$j9T$vLsh8tc6J1/NNTt8qKE2N/$1CVRI/qDhSNXeAz.tUO/W0UF4jmgmS5V81ZDhgw/Xo4";
        extraGroups = [ "wheel" ];
        shell = pkgs.nushell-wrapped;
        openssh.authorizedKeys.keys = [ keys.k900 ];
        description = "Ilya K";
      };
    };
  };

  home-manager = {
    useGlobalPkgs = true;
    users.k900.imports = [ ../home ];
    users.root.imports = [ ../home ];
    extraSpecialArgs = {
      inherit inputs;
    };
  };

  # build takes forever
  documentation.nixos.enable = false;

  # oomd
  systemd = {
    oomd = {
      enableRootSlice = true;
      enableUserSlices = true;
    };
    # always reserve some memory for sshd just in case
    services.sshd.serviceConfig.MemoryMin = "100M";
  };

  # enable nftables based firewall
  networking.nftables.enable = true;

  # enable sudo-rs instead of C sudo
  security = {
    sudo.enable = false;
    sudo-rs.enable = true;
  };

  # Keep track of versions better
  system = {
    nixos =
      let
        meta = rawInputs.nixpkgs;
      in
      {
        versionSuffix = ".${
          lib.substring 0 8 (meta.lastModifiedDate or meta.nixpkgs.lastModified or "19700101")
        }.${meta.shortRev or "dirty"}";
        revision = meta.rev or "dirty";
      };
    configurationRevision = inputs.self.rev or "dirty";

    # DO NOT try this at home
    stateVersion = lib.trivial.release;

    # Disable some unnecessary tools
    tools = {
      nixos-build-vms.enable = false;
      nixos-enter.enable = false;
      nixos-generate-config.enable = false;
      nixos-install.enable = false;
      nixos-option.enable = false;
    };
  };
}
