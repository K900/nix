{ config, ... }:
{
  system.activationScripts.diff.text = ''
    if [ -d "/run/current-system" ]; then
      echo -e "\e[1mChanges:\e[0m"
      ${config.nix.package}/bin/nix --experimental-features nix-command store diff-closures /run/current-system $systemConfig || echo "Error: $?"
      echo
    fi
  '';
}
