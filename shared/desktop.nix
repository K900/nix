{ pkgs, ... }:
{
  imports = [
    ./desktop-base.nix
    ./base/dev-tools.nix
  ];

  home-manager.users.k900.imports = [ ./home/graphical.nix ];

  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark-qt;
  };

  services = {
    printing = {
      enable = true;
      browsedConf = ''
        CreateIPPPrinterQueues Driverless
      '';
    };
    avahi.enable = true;
    ratbagd.enable = true;
  };

  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.sane-airscan ];
  };
}
