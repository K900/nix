{
  services = {
    cadvisor = {
      enable = true;
      port = 9002;
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "cadvisor";
        scrape_configs = [
          {
            job_name = "cadvisor";
            static_configs = [ { targets = [ "localhost:9002" ]; } ];
          }
        ];
      }
    ];
  };
}
