{
  services = {
    pgscv = {
      enable = true;
      logLevel = "debug";
      settings = {
        services.postgres = {
          service_type = "postgres";
          conninfo = "postgres://";
        };
      };
    };

    prometheus.exporters.postgres = {
      enable = true;
      port = 9003;
      runAsLocalSuperUser = true;
      extraFlags = [
        "--auto-discover-databases"
        "--collector.long_running_transactions"
        "--collector.stat_activity_autovacuum"
        "--collector.stat_statements"
      ];
    };

    postgresql.settings.shared_preload_libraries = "pg_stat_statements";

    grafana-agent.settings.metrics.configs = [
      {
        name = "postgres";
        scrape_configs = [
          {
            job_name = "postgres";
            static_configs = [ { targets = [ "localhost:9003" ]; } ];
          }
        ];
      }
      {
        name = "pgscv";
        scrape_configs = [
          {
            job_name = "pgscv";
            static_configs = [ { targets = [ "localhost:9890" ]; } ];
          }
        ];
      }
    ];
  };
}
