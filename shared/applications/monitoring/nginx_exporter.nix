{
  services = {
    prometheus.exporters.nginx = {
      enable = true;
      port = 9004;
      sslVerify = false;
    };

    grafana-agent.settings.metrics.configs = [
      {
        name = "nginx";
        scrape_configs = [
          {
            job_name = "nginx";
            static_configs = [ { targets = [ "localhost:9004" ]; } ];
          }
        ];
      }
    ];

  };
}
