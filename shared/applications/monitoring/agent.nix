{ config, ... }:
{
  services.grafana-agent = {
    enable = true;
    settings = {
      metrics.global = {
        remote_write = [ { url = "https://mimir.ts.0upti.me/api/v1/push"; } ];
        external_labels.hostname = config.networking.hostName;
      };
      logs = {
        global.clients = [ { url = "https://loki.ts.0upti.me/loki/api/v1/push"; } ];
        configs = [
          {
            name = "journald";
            scrape_configs = [
              {
                job_name = "system";
                journal = {
                  max_age = "12h";
                  labels = {
                    job = "systemd-journal";
                    host = config.networking.hostName;
                  };
                };
                relabel_configs = [
                  {
                    source_labels = [ "__journal__systemd_unit" ];
                    target_label = "unit";
                  }
                ];
              }
            ];
          }
        ];
        positions_directory = "\${STATE_DIRECTORY}/positions";
      };
      integrations.node_exporter.enable_collectors = [
        "processes"
        "systemd"
        "wifi"
      ];
    };
  };

  systemd.services.grafana-agent = {
    after = [
      "tailscaled.service"
      "network-online.target"
    ];
    wants = [ "network-online.target" ];
  };
}
