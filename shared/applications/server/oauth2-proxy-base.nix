{
  services = {
    redis.servers.oauth2_proxy = {
      enable = true;
      user = "oauth2-proxy";
    };

    oauth2-proxy = {
      enable = true;

      provider = "oidc";
      keyFile = "/run/agenix/oauth2_proxy_secrets";

      cookie.domain = ".0upti.me";
      email.domains = [ "*" ];

      extraConfig = {
        code-challenge-method = "S256";
        whitelist-domain = "*.0upti.me";
        reverse-proxy = true;
        scope = "openid email profile groups";
        session-store-type = "redis";
        redis-connection-url = "unix:/run/redis-oauth2_proxy/redis.sock";
      };
    };
  };
}
