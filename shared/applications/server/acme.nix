{
  users.groups.acme = { };

  age.secrets.dns.file = ../../../secrets/dns.age;

  security.acme = {
    defaults = {
      email = "me@0upti.me";
      dnsProvider = "cloudflare";
      dnsResolver = "1.1.1.1:53";
      environmentFile = "/run/agenix/dns";
    };

    acceptTerms = true;
  };
}
