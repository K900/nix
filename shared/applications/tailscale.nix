{ config, ... }:
{
  services.tailscale = {
    enable = true;
    useRoutingFeatures = "both";
  };

  systemd.network.wait-online.ignoredInterfaces = [ "tailscale0" ];

  networking.firewall.allowedUDPPorts = [ config.services.tailscale.port ];
}
