{
  nix.buildMachines =
    builtins.map
      (host: {
        protocol = "ssh-ng";
        hostName = host;
        system = "aarch64-linux";
        maxJobs = 2;
        speedFactor = 3;
        supportedFeatures = [
          "big-parallel"
          "kvm"
          "nixos-test"
        ];
      })
      [
        "qb"
        # "index"
      ];
}
