{
  config,
  inputs,
  pkgs,
  ...
}:
{
  imports = [
    ./base/common-config.nix
    ./base/dev-tools.nix
    inputs.nixos-wsl.nixosModules.wsl
  ];

  boot.bootspec.enable = false;

  wsl = {
    enable = true;
    defaultUser = "k900";
    startMenuLaunchers = false;

    # slows down completion way too much
    interop.includePath = false;

    wslConf.network.hostname = config.networking.hostName;
  };

  environment.systemPackages = [
    inputs.winhibit.packages."${pkgs.stdenv.hostPlatform.system}".winhibit
    pkgs.wslu
  ];

  networking.firewall.enable = false;

  # very shitty OOM killer, to make up for WSL not having PSI
  systemd.services.nix-daemon.serviceConfig.MemoryMax = "95%";

  time.timeZone = "Europe/Moscow";
}
