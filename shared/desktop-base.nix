{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    ./base/common-config.nix

    ./applications/tailscale.nix
    ./applications/monitoring

    inputs.bootis.nixosModules.default
    inputs.bootis.nixosModules.theme-regular

    ../hacks/kwallet-fdo.nix
  ];

  boot = {
    plymouth.enable = true;
    consoleLogLevel = 3;
    kernelParams = [ "quiet" ];

    loader.bootis = {
      enable = true;

      extraEntryConfig.graphics = "on";

      extraConfig = {
        resolution = "max";
        use_graphics_for = [
          "linux"
          "windows"
        ];

        showtools = [
          "memtest"
          "firmware"
        ];
        scanfor = [
          "manual"
          "external"
        ];
      };

      refind.theme.regular = {
        enable = true;
        color = "dark";
      };
    };
  };

  hardware = {
    bluetooth = {
      enable = true;
      settings.General.Experimental = true;
    };

    steam-hardware.enable = true;
    uinput.enable = true;
  };

  networking = {
    networkmanager = {
      enable = true;
      plugins = lib.mkForce [ ];
      wifi.backend = "iwd";
      connectionConfig."connection.mdns" = 2; # enabled
    };
    firewall.enable = false;
  };

  i18n.extraLocaleSettings = {
    LC_TIME = "en_DK.UTF-8";
    LC_NUMERIC = "ru_RU.UTF-8";
    LC_MONETARY = "ru_RU.UTF-8";
    LC_PAPER = "ru_RU.UTF-8";
    LC_TELEPHONE = "ru_RU.UTF-8";
  };

  services = {
    desktopManager.plasma6 = {
      enable = true;
      enableQt5Integration = false;
    };

    displayManager = {
      sddm = {
        enable = true;
        wayland.enable = true;
      };
      defaultSession = "plasma";
      autoLogin = {
        enable = true;
        user = "k900";
      };
    };

    pipewire = {
      enable = true;
      pulse.enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
    };

    resolved.enable = true;
    fstrim.enable = true;
  };

  security = {
    polkit.enable = true;
    rtkit.enable = true;
  };

  console.useXkbConfig = true;

  fonts = {
    packages = with pkgs; [
      cantarell-fonts
      dejavu_fonts
      fira-code
      noto-fonts
      noto-fonts-color-emoji
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif
      paratype-pt-sans
      paratype-pt-mono
      paratype-pt-serif
      roboto
    ];
    enableDefaultPackages = false;

    # force color emoji in terminal
    fontconfig.localConf = ''
      <?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
      <fontconfig>
        <match>
          <test name="family">
            <string>monospace</string>
          </test>
          <edit name="family" mode="append_last" binding="strong">
            <string>Noto Color Emoji</string>
          </edit>
        </match>
      </fontconfig>
    '';
  };

  environment = {
    sessionVariables.NIXOS_OZONE_WL = "1";

    systemPackages = with pkgs.kdePackages; [
      karousel
      kzones
    ];

    plasma6.excludePackages = with pkgs.kdePackages; [
      elisa
      kate
      khelpcenter
    ];
  };

  programs = {
    dconf.enable = true;
    kdeconnect.enable = true;
    kde-pim.enable = false;
    steam.enable = true;
  };

  users.users.k900.extraGroups = [ "pipewire" ];
  home-manager.users.k900.imports = [ ./home/graphical-base.nix ];

  time.timeZone = "Europe/Moscow";
}
