{
  config,
  lib,
  pkgs,
  pkgsHost,
  inputs,
  ...
}:
let
  crossPkgs = pkgsHost.pkgsCross.aarch64-multiplatform;

  uboot = crossPkgs.buildUBoot {
    src = inputs.uboot-bpi-r4;
    version = "2024.10-mtk";
    defconfig = "mt7988a_bananapi_bpi-r4-bootstd_defconfig";
    filesToInstall = [ "u-boot.bin" ];
  };

  tfA = crossPkgs.buildArmTrustedFirmware {
    platform = "mt7988";
    extraMakeFlags = [
      "BL33=${uboot}/u-boot.bin" # FIP-ify our uboot
      "BOOT_DEVICE=spim-nand" # boot from NAND flash
      "DRAM_USE_COMB=1" # you're supposed to use this one, sayeth mediatek
      "DDR4_4BG_MODE=0" # disable large RAM support, for some reason this breaks things
      "USE_MKIMAGE=1" # use uboot mkimage instead of vendor mtk tool
      "bl2"
      "fip"
    ];
    filesToInstall = [
      "build/mt7988/release/bl2.img"
      "build/mt7988/release/fip.bin"
    ];
  };

  tfA' = tfA.overrideAttrs (old: {
    src = pkgsHost.fetchFromGitHub {
      owner = "mtk-openwrt";
      repo = "arm-trusted-firmware";
      rev = "bacca82a8cac369470df052a9d801a0ceb9b74ca";
      hash = "sha256-n5D3styntdoKpVH+vpAfDkCciRJjCZf9ivrI9eEdyqw=";
    };
    nativeBuildInputs =
      old.nativeBuildInputs
      ++ (with pkgsHost; [
        dtc
        openssl
        ubootTools
        which
      ]);
  });

  uboot-combined = pkgsHost.runCommand "uboot.img" { } ''
    dd if=${tfA'}/bl2.img of=uboot.img
    # magic offset hardcoded in BL2 by default
    dd if=${tfA'}/fip.bin of=uboot.img conv=notrunc bs=512 seek=$((0x580000 / 512))

    mkdir $out
    mv uboot.img $out/
  '';

  kernel = crossPkgs.linuxManualConfig {
    version = "6.14.0-rc5";
    modDirVersion = "6.14.0-rc5";
    src = inputs.linux-bpi-r4;
    configfile = "${inputs.linux-bpi-r4}/arch/arm64/configs/mt7988a_bpi-r4_defconfig";
    allowImportFromDerivation = true;
  };
in
{
  boot = {
    kernelPackages = crossPkgs.linuxPackagesFor kernel;

    kernelParams = [ "console=ttyS0,115200n1" ];
    consoleLogLevel = 7;

    initrd = {
      kernelModules = [ "mmc_block" ];
      includeDefaultModules = false;
      systemd.tpm2.enable = false;
    };

    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };

    growPartition = true;
  };

  hardware = {
    enableRedistributableFirmware = true;

    deviceTree.overlays = [
      {
        name = "bpi-r4-emmc";
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "bananapi,bpi-r4";
          };

          &mmc0 {
            pinctrl-names = "default", "state_uhs";
            pinctrl-0 = <&mmc0_pins_emmc_51>;
            pinctrl-1 = <&mmc0_pins_emmc_51>;
            bus-width = <8>;
            max-frequency = <200000000>;
            cap-mmc-highspeed;
            mmc-hs200-1_8v;
            mmc-hs400-1_8v;
            hs400-ds-delay = <0x12814>;
            vqmmc-supply = <&reg_1p8v>;
            vmmc-supply = <&reg_3p3v>;
            non-removable;
            no-sd;
            no-sdio;
            status = "okay";
          };
        '';
      }
      {
        name = "bpi-r4-wireless";
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "bananapi,bpi-r4";
          };

          &{/} {
            wifi_12v: regulator-wifi-12v {
              compatible = "regulator-fixed";
              regulator-name = "wifi";
              regulator-min-microvolt = <12000000>;
              regulator-max-microvolt = <12000000>;
              gpio = <&pio 4 0>; // GPIO_ACTIVE_HIGH = 0
              enable-active-high;
              regulator-always-on;
            };
          };

          &i2c_wifi {
            status = "okay";

            // 5G WIFI MAC Address EEPROM
            wifi_eeprom@51 {
              compatible = "atmel,24c02";
              reg = <0x51>;
              address-bits = <8>;
              page-size = <8>;
              size = <256>;

              nvmem-layout {
                compatible = "fixed-layout";
                #address-cells = <1>;
                #size-cells = <1>;

                macaddr_5g: macaddr@0 {
                    reg = <0x0 0x6>;
                };
              };
            };

            // 6G WIFI MAC Address EEPROM
            wifi_eeprom@52 {
              compatible = "atmel,24c02";
              reg = <0x52>;
              address-bits = <8>;
              page-size = <8>;
              size = <256>;

              nvmem-layout {
                compatible = "fixed-layout";
                #address-cells = <1>;
                #size-cells = <1>;

                macaddr_6g: macaddr@0 {
                    reg = <0x0 0x6>;
                };
              };
            };
          };

          &pcie0 {
            pcie@0,0 {
              reg = <0x0000 0 0 0 0>;

              wifi@0,0 {
                compatible = "mediatek,mt76";
                reg = <0x0000 0 0 0 0>;
                nvmem-cell-names = "mac-address";
                nvmem-cells = <&macaddr_5g>;
              };
            };
          };

          &pcie1 {
            pcie@0,0 {
              reg = <0x0000 0 0 0 0>;

              wifi@0,0 {
                compatible = "mediatek,mt76";
                reg = <0x0000 0 0 0 0>;
                nvmem-cell-names = "mac-address";
                nvmem-cells = <&macaddr_6g>;
              };
            };
          };
        '';
      }
    ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  system.build = {
    sdImage = import "${inputs.nixpkgs}/nixos/lib/make-disk-image.nix" {
      name = "bpi-r4-sd-image";
      copyChannel = false;
      inherit config lib pkgs;
    };
    uboot = uboot-combined;
  };
}
