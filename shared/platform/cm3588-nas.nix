{
  imports = [ ./rk3588.nix ];

  hardware.deviceTree.name = "rockchip/rk3588-friendlyelec-cm3588-nas.dtb";
}
