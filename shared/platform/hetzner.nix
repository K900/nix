{ pkgs, modulesPath, ... }:
{
  imports = [ "${modulesPath}/profiles/qemu-guest.nix" ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    loader.grub = {
      enable = true;
      device = "/dev/sda";
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-partlabel/root";
      fsType = "bcachefs";
      options = [ "compression=zstd,version_upgrade=compatible,discard" ];
    };
    "/boot" = {
      device = "/dev/disk/by-partlabel/boot";
      fsType = "ext4";
    };
  };

  networking.useDHCP = false;

  systemd.network = {
    enable = true;

    networks.hetzner = {
      name = "enp1s0";
      gateway = [
        "172.31.1.1"
        "fe80::1"
      ];
      routes = [ { Destination = "172.31.1.1"; } ];
    };
  };
}
