{
  config,
  lib,
  pkgs,
  pkgsHost,
  inputs,
  ...
}:
let
  crossPkgs = pkgsHost.pkgsCross.aarch64-multiplatform;
in
{
  imports = [ ./rk3588.nix ];

  hardware.deviceTree.name = "rockchip/rk3588-orangepi-5-max.dtb";

  system.build = {
    diskImage = import "${inputs.nixpkgs}/nixos/lib/make-disk-image.nix" {
      name = "orangepi5max-disk-image";
      copyChannel = false;
      inherit config lib pkgs;
    };
    uboot = crossPkgs.callPackage ../../hacks/orangepi5max/uboot { };
  };
}
