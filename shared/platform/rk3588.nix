{ pkgsHost, ... }:
{
  boot = {
    kernelPackages = pkgsHost.pkgsCross.aarch64-multiplatform.linuxPackages_testing;

    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };

    growPartition = true;

    initrd.kernelModules = [
      "ahci_dwc"
      "phy_rockchip_naneng_combphy"
    ];
    consoleLogLevel = 7;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };
}
