{ lib, modulesPath, ... }:
{
  imports = [
    "${modulesPath}/installer/sd-card/sd-image-aarch64.nix"
  ];

  boot.supportedFilesystems.zfs = lib.mkForce false;

  hardware = {
    enableRedistributableFirmware = true;

    deviceTree.overlays = [
      {
        name = "rpi4-cma-overlay";
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "brcm,bcm2711";
          };

          &cma {
            size = <(512 * 1024 * 1024)>;
          };
        '';
      }
      {
        name = "rpi4-uart4-overlay";
        # see: https://github.com/torvalds/linux/blob/master/arch/arm/boot/dts/broadcom/bcm2711.dtsi
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "brcm,bcm2711";
          };

          &uart4 {
            pinctrl-names = "default";
            pinctrl-0 = <&uart4_gpio8 &uart4_ctsrts_gpio10>;
            status = "okay";
          };
        '';
      }
      {
        name = "rpi4-dwc2-overlay";
        # see: https://github.com/torvalds/linux/blob/master/arch/arm/boot/dts/broadcom/bcm283x-rpi-usb-otg.dtsi
        dtsText = ''
          /dts-v1/;
          /plugin/;

          / {
            compatible = "brcm,bcm2711";
          };

          &usb {
            dr_mode = "otg";
            g-rx-fifo-size = <256>;
            g-np-tx-fifo-size = <32>;
            g-tx-fifo-size = <256 256 512 512 512 768 768>;
            status = "okay";
          };
        '';
      }
    ];
  };

  boot = {
    kernelModules = [ "g_serial" ];
    kernelParams = lib.mkForce [ "console=ttyS1,115200" ];
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };
  };

  systemd.services."serial-getty@ttyGS0" = {
    wantedBy = [ "getty.target" ];
    serviceConfig.Restart = "always";
  };

  powerManagement.cpuFreqGovernor = "ondemand";
}
