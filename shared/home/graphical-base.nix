{
  config,
  inputs,
  pkgs,
  ...
}:
{
  imports = [
    inputs.plasma-manager.homeManagerModules.plasma-manager
    inputs.spicetify-nix.homeManagerModules.default
  ];

  home = {
    packages = with pkgs; [
      vscode

      kdePackages.skanpage
      kdePackages.yakuake

      arc-kde-theme # for yakuake

      ktailctl
      syncthingtray

      piper
    ];
  };

  programs = {
    firefox = {
      enable = true;
      package = inputs.firefox.packages.${pkgs.stdenv.hostPlatform.system}.firefox-nightly-bin;
      nativeMessagingHosts = [ pkgs.kdePackages.plasma-browser-integration ];
    };

    mpv = {
      enable = true;
      defaultProfiles = [ "gpu-hq" ];
      scripts = [ pkgs.mpvScripts.uosc ];
      config = {
        hwdec = "auto";
        ytdl-format = "bestvideo+bestaudio";
      };
    };

    konsole = {
      enable = true;
      customColorSchemes."One Dark" = ./one-dark.colorscheme;
      profiles.default = {
        colorScheme = "One Dark";
        font = {
          name = "Fira Code";
          size = 13;
        };
        extraConfig.General.StartInCurrentSessionDir = false;
      };
      defaultProfile = "default";
    };

    plasma = {
      enable = true;

      input.keyboard = {
        layouts = [
          { layout = "us"; }
          { layout = "ru"; }
        ];
        options = [ "grp:caps_toggle" ];
      };

      spectacle.shortcuts.captureRectangularRegion = "Meta+Shift+S";

      workspace = {
        colorScheme = "BreezeDark";
        cursor.theme = "Breeze_Light";
        lookAndFeel = "org.kde.breezedark.desktop";
      };

      configFile = {
        # let HM manage GTK settings
        kded5rc."Module-gtkconfig".autoload = false;

        # don't restore windows on startup
        ksmserverrc.General.loginMode = "emptySession";

        # kzones stuff
        kwinrc = {
          Plugins.kzonesEnabled = true;
          Script-kzones = {
            layoutsJson = builtins.toJSON [
              {
                name = "Priority Grid";
                padding = 16;
                zones = [
                  {
                    x = 0;
                    y = 0;
                    height = 100;
                    width = 25;
                  }
                  {
                    x = 25;
                    y = 0;
                    height = 100;
                    width = 50;
                  }
                  {
                    x = 75;
                    y = 0;
                    height = 100;
                    width = 25;
                  }
                ];
              }
              {
                name = "Quadrant Grid";
                padding = 16;
                zones = [
                  {
                    x = 0;
                    y = 0;
                    height = 50;
                    width = 50;
                  }
                  {
                    x = 0;
                    y = 50;
                    height = 50;
                    width = 50;
                  }
                  {
                    x = 50;
                    y = 50;
                    height = 50;
                    width = 50;
                  }
                  {
                    x = 50;
                    y = 0;
                    height = 50;
                    width = 50;
                  }
                ];
              }
              {
                name = "Side by side";
                padding = 16;
                zones = [
                  {
                    x = 0;
                    y = 0;
                    height = 100;
                    width = 50;
                  }
                  {
                    x = 50;
                    y = 0;
                    height = 100;
                    width = 50;
                  }
                ];
              }
            ];
            trackLayoutPerScreen = true;
            zoneOverlayHighlightTarget = 1;
            zoneOverlayIndicatorDisplay = 1;
          };
        };

        # make spectacle copy to clipboard by default
        spectaclerc = {
          General.clipboardGroup = "PostScreenshotCopyImage";
          ImageSave.imageSaveLocation = config.home.homeDirectory;
          VideoSave.videoSaveLocation = config.home.homeDirectory;
        };

        # make yakuake look nice
        yakuakerc = {
          Appearance.Skin = "arc-dark";
          "Desktop Entry".DefaultProfile = "default.profile";
          Dialogs.FirstRun = false;
          Window = {
            Height = 80;
            KeepOpen = false;
          };
        };
      };
    };
  };

  xdg = {
    mimeApps = {
      enable = true;
      defaultApplications = {
        "text/markdown" = "code.desktop";
        "text/plain" = "code.desktop";
        "text/x-csv" = "code.desktop";
        "text/x-log" = "code.desktop";
        "text/x-patch" = "code.desktop";
        "text/html" = "firefox-nightly.desktop";
        "x-scheme-handler/http" = "firefox-nightly.desktop";
        "x-scheme-handler/https" = "firefox-nightly.desktop";
        "x-scheme-handler/mailto" = "firefox-nightly.desktop";
        "application/pdf" = "org.kde.okular.desktop";
        "application/xml" = "code.desktop";
        "application/x-yaml" = "code.desktop";
        "image/avif" = "org.kde.gwenview.desktop";
        "image/bmp" = "org.kde.gwenview.desktop";
        "image/heif" = "org.kde.gwenview.desktop";
        "image/jpeg" = "org.kde.gwenview.desktop";
        "image/png" = "org.kde.gwenview.desktop";
        "image/webp" = "org.kde.gwenview.desktop";
        "image/x-icns" = "org.kde.gwenview.desktop";
        "inode/directory" = "org.kde.dolphin.desktop";
      };
    };
    userDirs = {
      enable = true;
      desktop = "$HOME/";
      documents = "$HOME/";
      download = "$HOME/downloads";
      music = "$HOME/";
      pictures = "$HOME/";
      publicShare = "$HOME/";
      templates = "$HOME/";
      videos = "$HOME/";
    };

    configFile = {
      "mimeapps.list".force = true;
      "autostart/org.kde.yakuake.desktop".source =
        "${pkgs.kdePackages.yakuake}/share/applications/org.kde.yakuake.desktop";
    };
  };

  gtk = {
    enable = true;
    theme = {
      package = pkgs.kdePackages.breeze-gtk;
      name = "Breeze-Dark";
    };
    cursorTheme = {
      package = pkgs.kdePackages.breeze;
      name = "Breeze_Light";
    };
    iconTheme = {
      package = pkgs.kdePackages.breeze-icons;
      name = "breeze-dark";
    };
    gtk2 = {
      configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
      extraConfig = ''
        gtk-alternative-button-order = 1
      '';
    };
    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = true;
      gtk-decoration-layout = "icon:minimize,maximize,close";
    };
    gtk4.extraConfig = {
      gtk-application-prefer-dark-theme = true;
      gtk-decoration-layout = "icon:minimize,maximize,close";
    };
  };

  services.syncthing.enable = true;
}
