let maybe_nom = if (which nom | is-not-empty) {
  ["--nom"]
} else {
  []
}

def --wrapped nix-shell (...args) {
  nix-your-shell nu ...$maybe_nom nix-shell -- ...$args
}

def --wrapped nix (...args) {
  nix-your-shell nu ...$maybe_nom nix -- ...$args
}

# https://www.nushell.sh/cookbook/external_completers.html#fish-completer
# let external_completer = {|spans|
#     # if the current command is an alias, get it's expansion
#     let expanded_alias = (scope aliases | where name == $spans.0 | get -i 0 | get -i expansion)

#     # overwrite
#     let spans = (if $expanded_alias != null  {
#         # put the first word of the expanded alias first in the span
#         $spans | skip 1 | prepend ($expanded_alias | split words)
#     } else { $spans })

#     fish --command $'complete "--do-complete=($spans | str join " ")"'
#     | $"value(char tab)description(char newline)" + $in
#     | from tsv --flexible --no-infer
# }

def "history fzf" [] {
  try {
    commandline edit (
      history
        | each { |it| $it.command }
        | reverse
        | uniq
        | str join (char -i 0)
        | fzf --read0 --height=40% --scheme=history -q (commandline)
        | decode utf-8
        | str trim
    )
  } catch {
    # probably exited fzf
    return
  }
}

def "pwd short" [] {
  try {
    let short = pwd | path relative-to ("~" | path expand)
    if $short == "" {
      return "~"
    } else {
      return $"~/($short)"
    }
  } catch {
    return (pwd)
  }
}

$env.config = {
  completions: {
    algorithm: "fuzzy"
    # external: {
    #   enable: true,
    #   completer: $external_completer
    # }
  }
  show_banner: false
  color_config: {
    separator: "#abb2bf"
    leading_trailing_space_bg: { attr: "n" }
    header: { fg: "#98c379" attr: "b" }
    empty: "#61afef"
    bool: {|| if $in { "#56b6c2" } else { "light_gray" } }
    int: "#abb2bf"
    filesize: {|e|
      if $e == 0b {
        "#abb2bf"
      } else if $e < 1mb {
        "#56b6c2"
      } else {{ fg: "#61afef" }}
    }
    duration: "#abb2bf"
    date: {|| (date now) - $in |
      if $in < 1hr {
        { fg: "#e06c75" attr: "b" }
      } else if $in < 6hr {
        "#e06c75"
      } else if $in < 1day {
        "#d19a66"
      } else if $in < 3day {
        "#98c379"
      } else if $in < 1wk {
        { fg: "#98c379" attr: "b" }
      } else if $in < 6wk {
        "#56b6c2"
      } else if $in < 52wk {
        "#61afef"
      } else { "dark_gray" }
    }
    range: "#abb2bf"
    float: "#abb2bf"
    string: "#abb2bf"
    nothing: "#abb2bf"
    binary: "#abb2bf"
    cellpath: "#abb2bf"
    row_index: { fg: "#98c379" attr: "b" }
    record: "#abb2bf"
    list: "#abb2bf"
    block: "#abb2bf"
    hints: "dark_gray"
    search_result: { fg: "#e06c75" bg: "#abb2bf" }

    shape_and: { fg: "#c678dd" attr: "b" }
    shape_binary: { fg: "#c678dd" attr: "b" }
    shape_block: { fg: "#61afef" attr: "b" }
    shape_bool: "#56b6c2"
    shape_custom: "#98c379"
    shape_datetime: { fg: "#56b6c2" attr: "b" }
    shape_directory: "#56b6c2"
    shape_external: "#56b6c2"
    shape_externalarg: { fg: "#98c379" attr: "b" }
    shape_filepath: "#56b6c2"
    shape_flag: { fg: "#61afef" attr: "b" }
    shape_float: { fg: "#c678dd" attr: "b" }
    shape_garbage: { fg: "#FFFFFF" bg: "#FF0000" attr: "b" }
    shape_globpattern: { fg: "#56b6c2" attr: "b" }
    shape_int: { fg: "#c678dd" attr: "b" }
    shape_internalcall: { fg: "#56b6c2" attr: "b" }
    shape_list: { fg: "#56b6c2" attr: "b" }
    shape_literal: "#61afef"
    shape_match_pattern: "#98c379"
    shape_matching_brackets: { attr: "u" }
    shape_nothing: "#56b6c2"
    shape_operator: "#d19a66"
    shape_or: { fg: "#c678dd" attr: "b" }
    shape_pipe: { fg: "#c678dd" attr: "b" }
    shape_range: { fg: "#d19a66" attr: "b" }
    shape_record: { fg: "#56b6c2" attr: "b" }
    shape_redirection: { fg: "#c678dd" attr: "b" }
    shape_signature: { fg: "#98c379" attr: "b" }
    shape_string: "#98c379"
    shape_string_interpolation: { fg: "#56b6c2" attr: "b" }
    shape_table: { fg: "#61afef" attr: "b" }
    shape_variable: "#c678dd"

    background: "#1e2127"
    foreground: "#5c6370"
    cursor: "#5c6370"
  }
  menus: [
    {
      name: completion_menu
      only_buffer_difference: false
      marker: ""
      type: {
        layout: columnar
        columns: 1
        col_padding: 2
      }
      style: {
        text: "#abb2bf"
        selected_text: { fg: "#dcdfe4", bg: "#5c6370" }
        description_text: "#56b6c2"
      }
    }
  ]
  keybindings: [
    # stolen from https://github.com/nushell/nushell/issues/1616
    {
      name: fuzzy_history
      modifier: control
      keycode: char_r
      mode: [emacs, vi_normal, vi_insert]
      event: [
        {
          send: ExecuteHostCommand
          cmd: "history fzf"
        }
      ]
    }
    {
      name: up_fuzzy_history
      modifier: none
      keycode: up
      mode: [emacs, vi_normal, vi_insert]
      event: {
         until: [
          {
            send: MenuUp
          },
          {
            send: ExecuteHostCommand
            cmd: "history fzf"
          }
         ]
      }
    }
  ]
  shell_integration: {
    # use custom shell title
    osc2: false
  }
  hooks: {
    pre_prompt: [{
      let title = if "SSH_CLIENT" in $env {
        $"(whoami)@(hostname):(pwd short)>"
      } else {
        $"(pwd short)>"
      }

      print -n $"(ansi title)($title)(ansi st)"
    }]
    pre_execution: [{
      let title = if "SSH_CLIENT" in $env {
        $"(whoami)@(hostname):(pwd short)> (commandline)"
      } else {
        $"(pwd short)> (commandline)"
      }

      print -n $"(ansi title)($title)(ansi st)"
    }]

    command_not_found: {|cmd|
      if (which nix-locate | is-empty) {
        return null
      }

      let pkgs = (nix-locate $"/bin/($cmd)" --whole-name --at-root --top-level --minimal)
      if ($pkgs | is-empty) {
        return null
      }

      return (
        $"(ansi $env.config.color_config.shape_external)($cmd)(ansi reset) " +
        $"may be found in the following packages:\n($pkgs)"
      )
    }
  }
}

$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
    to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
  }
  "WSLPATH": {
    from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
    to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
  }
}

# all very WIP
alias ll = ls -la

def --wrapped up (...args) {
  let realFlake = ("/etc/nixos/flake.nix" | path expand | path dirname)
  cd $realFlake

  git pull origin master --rebase

  nix flake update --commit-lock-file
  git push

  if ("/etc/NIXOS" | path exists) {
    nix develop --command colmena apply-local --sudo ...$args
  } else {
    home-manager switch --flake $realFlake ...$args
  }
}

def --wrapped win (cmd, ...rest) {
  let input = $in
  if "WSLPATH" in $env {
    $env.PATH = ($env.PATH | append $env.WSLPATH)
  }
  $input | run-external $cmd ...$rest
}

def bump [] {
  if ("flake.lock" | path exists) { nix flake update }
  if ("Cargo.lock" | path exists) { cargo update }
  if ("poetry.lock" | path exists) { poetry update -vvv --lock }
  if ("uv.lock" | path exists) { uv lock --refresh --upgrade }
  if ("yarn.lock" | path exists) { yarn upgrade }
  if (".envrc" | path exists) { direnv reload }
  if ((".pre-commit-config.yaml" | path exists) and (".pre-commit-config.yaml" | path type) != symlink) {
    pre-commit install
    pre-commit autoupdate
  }
  if (".git" | path exists) { git ci -m "chore: bump" }
}

alias code = win code
alias winget = win winget.exe

$env.NIX_PATH = "nixpkgs=flake:nixpkgs";
$env.NIXPKGS_ALLOW_UNFREE = "1";

$env.EDITOR = 'micro'

# FIXME: hack to make RBW not try to spawn its agent
$env.RBW_AGENT = "true";

# shut up direnv
$env.DIRENV_LOG_FORMAT = "";

if ("~/.local/bin" | path exists) {
  $env.PATH = $env.PATH | append "~/.local/bin"
}

try {
  if ("~/.config/rbw/config.json" | path exists) {
    systemctl --user start rbw-agent
    rbw unlock

    if not (try { ssh-add -l | str contains "stdin" } catch { false }) {
      rbw get 'SSH key' | ssh-add -
    }

    $env.CACHIX_AUTH_TOKEN = (rbw get Cachix)
    $env.GITLAB_TOKEN = (rbw get "Gitlab token")
    $env.GITHUB_TOKEN = (rbw get "Github token")
    $env.NIX_CONFIG = $"access-tokens = github.com=($env.GITHUB_TOKEN) gitlab.com=PAT:($env.GITLAB_TOKEN)"
  }
}
