{
  lib,
  config,
  pkgs,
  ...
}:
{
  home = {
    # DO NOT try this at home
    stateVersion = lib.trivial.release;

    # shut up prezto
    file.".zlogout".text = lib.mkForce "";

    packages = with pkgs; [
      # used by micro
      editorconfig-core-c

      # misc utils
      doggo
      erdtree
      fd
      isd
      jq
      lurk
      rsync
      trippy

      nix-your-shell
    ];

    # FIXME: hack to reload dbus activated things
    activation.reloadDbus = lib.hm.dag.entryAfter [ "reloadSystemd" ] ''
      if [[ -v DBUS_SESSION_BUS_ADDRESS ]]; then
        run ${pkgs.systemd}/bin/busctl --user call org.freedesktop.DBus /org/freedesktop/DBus org.freedesktop.DBus ReloadConfig
      fi
    '';
  };

  programs = {
    micro = {
      enable = true;
      settings = {
        clipboard = "internal";
        colorscheme = "one-dark";
        diffgutter = true;
        indentchar = "space";
        scrollbar = true;
      };
    };

    bat = {
      enable = true;
      config.theme = "OneHalfDark";
    };

    eza = {
      enable = true;
      extraOptions = [
        "--classify"
        "--color-scale"
        "--git"
        "--group-directories-first"
      ];
    };

    htop = {
      enable = true;
      settings =
        {
          tree_view = 1;
          hide_kernel_threads = 1;
          hide_userland_threads = 1;
          shadow_other_users = 1;
          show_thread_names = 1;
          show_program_path = 0;
          highlight_base_name = 1;
          header_layout = "two_67_33";
          color_scheme = 6;
        }
        // (with config.lib.htop; leftMeters [ (bar "AllCPUs4") ])
        // (
          with config.lib.htop;
          rightMeters [
            (bar "Memory")
            (bar "Swap")
            (bar "DiskIO")
            (bar "NetworkIO")
            (text "Systemd")
            (text "Tasks")
            (text "LoadAverage")
          ]
        );
    };

    zellij = {
      enable = true;
      enableBashIntegration = false;

      settings = {
        theme = "one-half-dark";
        themes.one-half-dark = {
          bg = [
            40
            44
            52
          ];
          gray = [
            40
            44
            52
          ];
          red = [
            227
            63
            76
          ];
          green = [
            152
            195
            121
          ];
          yellow = [
            229
            192
            123
          ];
          blue = [
            97
            175
            239
          ];
          magenta = [
            198
            120
            221
          ];
          orange = [
            216
            133
            76
          ];
          fg = [
            220
            223
            228
          ];
          cyan = [
            86
            182
            194
          ];
          black = [
            27
            29
            35
          ];
          white = [
            233
            225
            254
          ];
        };
      };
    };

    zoxide.enable = true;

    starship = {
      enable = true;
      settings =
        let
          darkgray = "242";
        in
        {
          format = lib.concatStrings [
            "$username"
            "($hostname )"
            "$directory"
            "($git_branch )"
            "($git_state )"
            "($git_status )"
            "($git_metrics )"
            "($cmd_duration )"
            "$line_break"
            "($python )"
            "($nix_shell )"
            "($direnv )"
            "$character"
          ];

          directory = {
            style = "blue";
            read_only = " !";
          };

          character = {
            success_symbol = "[❯](purple)";
            error_symbol = "[❯](red)";
            vicmd_symbol = "[❮](green)";
          };

          git_branch = {
            format = "[$branch]($style)";
            style = darkgray;
          };

          git_status = {
            format = "[[(*$conflicted$untracked$modified$staged$renamed$deleted )](218)($ahead_behind$stashed)]($style)";
            style = "cyan";
            conflicted = "​";
            untracked = "​";
            modified = "​";
            staged = "​";
            renamed = "​";
            deleted = "​";
            stashed = "≡";
          };

          git_state = {
            format = "[$state( $progress_current/$progress_total)]($style)";
            style = "dimmed";
            rebase = "rebase";
            merge = "merge";
            revert = "revert";
            cherry_pick = "pick";
            bisect = "bisect";
            am = "am";
            am_or_rebase = "am/rebase";
          };

          git_metrics.disabled = false;

          cmd_duration = {
            format = "[$duration]($style)";
            style = "yellow";
          };

          python = {
            format = "[$virtualenv]($style)";
            style = darkgray;
          };

          nix_shell = {
            format = "❄️";
            style = darkgray;
            heuristic = true;
          };

          direnv = {
            format = "[($loaded/$allowed)]($style)";
            style = darkgray;
            disabled = false;
            loaded_msg = "";
            allowed_msg = "";
          };

          username = {
            format = "[$user]($style)";
            style_root = "yellow italic";
            style_user = darkgray;
          };

          hostname = {
            format = "@[$hostname]($style)";
            style = darkgray;
          };
        };
    };

    # for nushell wrapper to pick up HM things
    bash.enable = true;
    # fish.enable = true; # only for nushell completions really
    carapace.enable = true;
    nushell = {
      enable = true;
      configFile.source = ./config.nu;
    };

    fzf = {
      enable = true;
      defaultOptions = [
        "--color=dark"
        "--color=fg:-1,bg:-1,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe"
        "--color=info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef"
      ];
    };

    man.enable = lib.mkDefault false;

    git = {
      enable = true;

      userName = "K900";
      userEmail = "me@0upti.me";

      aliases = {
        a = "add";
        br = "branch";
        cc = "commit -v";
        ci = "commit -a -v";
        co = "checkout";
        d = "diff";
        st = "status -s";

        ls = ''log --pretty=format:"%C(yellow)%h%Cred%d %Creset%s%Cblue [%cn]" --decorate'';
        ll = ''log --pretty=format:"%C(yellow)%h%Cred%d %Creset%s%Cblue [%cn]" --decorate --numstat'';

        filelog = "log -u";
        fl = "log -u";

        dl = "!git ll -1";

        dr = ''!f() { git diff "$1"^.."$1"; }; f'';

        fixup = "commit --fixup HEAD";
        fix = "rebase -i --autosquash";

        rhh = "reset --hard HEAD";
        alias = "config --get-regexp ^alias\\.";

        slurp = ''!f() { git add "$@"; git absorb --and-rebase; }; f'';
      };

      extraConfig = {
        color.ui = "auto";
        diff.algorithm = "histogram";
        merge.conflictstyle = "zdiff3";
        credential.helper = "store";
        init.defaultBranch = "main";

        branch.autosetuprebase = "always";
        pull.rebase = "merges";
        rebase.autostash = true;
        rebase.updateRefs = true;

        fetch = {
          prune = true;
          recurseSubmodules = true;
        };

        commit.verbose = true;
        rerere.enabled = true;

        branch.sort = "-committerdate";
        tag.sort = "-taggerdate";
        log.date = "iso";

        push = {
          default = "current";
          autosetupremote = true;
        };

        submodule.recurse = true;
        push.recurseSubmodules = false;
        status.submoduleSummary = true;
        diff.submodule = "log";

        url = {
          "ssh://aur@aur.archlinux.org/".insteadOf = "aur:";
          "ssh://git@codeberg.org/".insteadOf = "cb:";
          "ssh://git@gitlab.freedesktop.org/".insteadOf = "fdo:";
          "ssh://git@github.com/".insteadOf = "gh:";
          "ssh://git@gitlab.com/".insteadOf = "gl:";
          "ssh://git@invent.kde.org/".insteadOf = "kde:";
          "ssh://git@git.lix.systems/".insteadOf = "lix:";
        };

        sendemail = {
          smtpuser = "me@0upti.me";
          smtpserver = "smtp.yandex.ru";
          smtpencryption = "tls";
          smtpserverport = 587;
        };
      };
    };

    ripgrep = {
      enable = true;
      arguments = [
        "--smart-case"
        "--pretty"
      ];
    };
  };

  manual.manpages.enable = false;
}
