{
  lib,
  pkgs,
  ...
}:
{
  imports = [
    ./default.nix
    ./dev-tools.nix
    ../base/nix-settings.nix
    ../magic-gc/home.nix
    ../../hacks/nushell-wrapper.nix
  ];

  programs.home-manager.enable = true;
  nix.package = pkgs.lix;

  # install nushell wrapper as a normal package on standalone
  home.packages = [
    (lib.hiPrio pkgs.nushell-wrapped)
  ];
}
