{
  inputs,
  pkgs,
  ...
}:
{
  imports = [ inputs.nix-index-database.hmModules.nix-index ];

  home = {
    packages = with pkgs; [
      libarchive # for bsdtar
      bat
      dua
      lls

      wget # vscode server

      python3
      poetry
      uv

      nil

      nixpkgs-review
      nix-update
      nix-diff
      nix-init
      nix-tree
      nixfmt-rfc-style
      nix-output-monitor
      hydra-check

      gitoxide
      glab
      git-revise
      git-absorb
      git-gr

      linuxPackages_latest.perf

      wcurl

      ast-grep
    ];
  };

  programs = {
    git.difftastic = {
      enable = true;
      background = "dark";
      display = "inline";
    };

    rbw = {
      enable = true;
      settings = {
        email = "me@0upti.me";
        base_url = "https://bw.0upti.me/";
        lock_timeout = 60 * 60 * 24;
        pinentry = pkgs.pinentry-tty;
      };
    };

    ssh = {
      enable = true;
      matchBlocks = {
        "r0" = {
          hostname = "0upti.me";
          user = "root";
          forwardAgent = true;
        };

        "konata" = {
          hostname = "konata.0upti.me";
          user = "root";
          forwardAgent = true;
        };

        "yui" = {
          hostname = "yui";
          user = "root";
          forwardAgent = true;
        };

        "qb" = {
          hostname = "qb";
          user = "root";
          forwardAgent = true;
        };

        "morgana" = {
          hostname = "morgana";
          user = "root";
          forwardAgent = true;
        };

        "index" = {
          hostname = "index";
          user = "root";
          forwardAgent = true;
        };

        "gura" = {
          hostname = "gura";
          user = "root";
          forwardAgent = true;
        };

        "bananya" = {
          hostname = "bananya";
          user = "root";
          forwardAgent = true;
        };
      };
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    gh = {
      enable = true;
      gitCredentialHelper.enable = true;
    };

    man.enable = true;
  };

  systemd.user.services = {
    rbw-agent = {
      Unit.Description = "rbw agent";

      Service = {
        Type = "simple";
        RuntimeDirectory = "rbw";
        ExecStartPre = "${pkgs.coreutils}/bin/mkdir -p %h/.local/share/rbw/";
        ExecStart = "${pkgs.rbw}/bin/rbw-agent --no-daemonize";
      };

      Install.WantedBy = [ "default.target" ];
    };

    adb-server = {
      Unit.Description = "adb server daemon";

      Service = {
        Type = "simple";
        ExecStart = "${pkgs.android-tools}/bin/adb -a nodaemon server start";
      };

      Install.WantedBy = [ "default.target" ];
    };
  };
}
