{ inputs, pkgs, ... }:
{
  imports = [ ./graphical-base.nix ];

  home = {
    packages = with pkgs; [
      anytype

      telegram-desktop
      libreoffice-qt6-fresh

      qbittorrent
      yt-dlp
    ];
  };

  programs.spicetify =
    let
      spicePkgs = inputs.spicetify-nix.legacyPackages.${pkgs.stdenv.hostPlatform.system};
    in
    {
      enable = true;
      theme = spicePkgs.themes.sleek;
      colorScheme = "Nord";

      enabledCustomApps = with spicePkgs.apps; [
        lyricsPlus
        newReleases
      ];

      enabledExtensions = with spicePkgs.extensions; [
        adblock
        fullAppDisplayMod
        goToSong
        history
      ];
    };
}
